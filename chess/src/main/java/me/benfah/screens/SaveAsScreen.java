package me.benfah.screens;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import me.benfah.Chess;
import me.benfah.logic.IChessGame;
import me.benfah.logic.Serializer;
import me.benfah.utils.ScreenHandler;
import me.benfah.utils.Serializers;
import processing.core.PGraphics;

public class SaveAsScreen extends Screen
{
	
	IChessGame game;
	String inputText = "game-" + System.currentTimeMillis() + ".bcg";
	boolean saveErr = false;
	
	public SaveAsScreen(IChessGame game) {
		this.game = game;
	}
	
	@Override
	public void draw(PGraphics g) {
		getGraphics().background(256, 256, 256);
		
		g.fill(0);
		g.textSize(50);
		g.text(inputText.isEmpty() ? "Enter filename..." : inputText, getWidth() / 2, getHeight() / 2);
		
		g.textSize(30);
		g.text(saveErr ? "Too short or already exists" : "Change name and/or hit ENTER", getWidth() / 2, getHeight() / 2 - 80);

		super.draw(g);
	}
	
	@Override
	public void keyPressed() {
		char key = Chess.getInstance().key;
		if(key == '\n') {
			try {
				saveErr = writeToFile();
				if(!saveErr)
					ScreenHandler.setScreen(new MainScreen());
			} catch (Exception e) {
				inputText = "";
			}
		} else if(key == '\u0008' && inputText.length() > 0) {
			inputText = inputText.substring(0, inputText.length() - 1);
		} else if(Chess.font.getGlyph(Chess.getInstance().key) != null) {
			inputText += Chess.getInstance().key;

		}
	}
	
	boolean writeToFile() {
		if(inputText.length() > 3 || new File(inputText).exists())
			return false;
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(new File(inputText)));
			bw.write(Serializers.CHESS_WITH_HISTORY.serialize(game));
			bw.close();
			return true;
		} catch (IOException e) {
		}
		return false;
	}
	
}
