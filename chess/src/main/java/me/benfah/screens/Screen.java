package me.benfah.screens;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import me.benfah.Chess;
import me.benfah.screens.components.IComponent;
import processing.core.PGraphics;

public abstract class Screen {
	
	List<IComponent> interactables = new ArrayList<>();
	
    public void draw(PGraphics g) {
    	for(IComponent i : interactables) {
    		i.onDraw();
    	}
    }
    
    public void mouseClicked() {
    	for(int i = interactables.size() - 1; i >= 0; i--) {
    		if(interactables.get(i).isInBounds(getMouseX(), getMouseY())) {
    			interactables.get(i).onClick();
    			break;
    		}
    	}
    };
    
    public void mouseMoved() {
    	for(int i = interactables.size() - 1; i >= 0; i--) {
    		if(interactables.get(i).isInBounds(getMouseX(), getMouseY())) {
    			interactables.get(i).onHover();
    			break;
    		}
    	}
    }; 
    
    public void keyPressed() {}
    
    public void handleFile(File f) {}
    
    public int getWidth() {
    	return Chess.getInstance().width;
    }
    
    public int getHeight() {
    	return Chess.getInstance().height;
    }
    
    public int getMouseX() {
    	return Chess.getInstance().mouseX;
    }
    
    public int getMouseY() {
    	return Chess.getInstance().mouseY;
    }
    
    public PGraphics getGraphics() {
    	return Chess.getInstance().getGraphics();
    }
    
    public void addInteractable(IComponent i) {
    	interactables.add(i);
    }
}
