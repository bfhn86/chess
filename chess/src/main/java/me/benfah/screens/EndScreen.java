package me.benfah.screens;

import me.benfah.network.PacketHandler;
import me.benfah.screens.components.Button;
import me.benfah.utils.ScreenHandler;
import processing.core.PGraphics;

public class EndScreen extends Screen {
	static int BUTTON_WIDTH = 200;
	static int BUTTON_HEIGHT = 50;
	Button b;
	
	String title, subtitle;
	
	public EndScreen(String title, String subtitle) {
		this.title = title;
		this.subtitle = subtitle;
		b = new Button(getWidth() / 2 - BUTTON_WIDTH / 2, 300, 200, 50, "Menu", this::onPlayClick);
		addInteractable(b);
	}
	
	@Override
	public void draw(PGraphics g) {
		getGraphics().background(256, 256, 256);
		g.textSize(100);
		g.fill(0);
		g.text(title, getWidth() / 2, 150);
		g.textSize(50);
		g.text(subtitle, getWidth() / 2, 240);

		super.draw(g);

	}
	
	public void onPlayClick() {
		ScreenHandler.setScreen(new MainScreen());
		if(PacketHandler.getChannel() != null) PacketHandler.getChannel().close();
	}
	
}
