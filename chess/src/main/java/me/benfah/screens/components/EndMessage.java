package me.benfah.screens.components;

public class EndMessage implements IComponent{
	
	String title, subtitle;
	
	public EndMessage(String title, String subtitle) {
		this.title = title;
		this.subtitle = subtitle;
	}
	
	@Override
	public boolean isInBounds(int x, int y) {
		return true;
	}

	@Override
	public void onClick() {
		
	}

	@Override
	public void onDraw() {
		
	}

	@Override
	public void onHover() {
		
	}
	
}
