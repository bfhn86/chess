package me.benfah.screens.components;

import me.benfah.Chess;
import processing.core.PApplet;
import processing.core.PGraphics;

public class Button implements IComponent {

	int width, height, x, y, fontSize;
	Runnable onClick;
	int animationTime;
	boolean inBounds = false;
	String text;
	
	public Button(int x, int y, int width, int height, String text, Runnable onClick) {
		this(x, y, width, height, text, 40, onClick);
	}
	
	public Button(int x, int y, int width, int height, String text, int fontSize, Runnable onClick) {
		this.width = width;
		this.height = height;
		this.x = x;
		this.y = y;
		this.onClick = onClick;
		this.text = text;
		this.fontSize = fontSize;
	}

	@Override
	public boolean isInBounds(int x, int y) {
		return x >= this.x && y >= this.y && x <= this.x + width && y <= this.y + height;
	}

	@Override
	public void onClick() {
		onClick.run();
	}

	@Override
	public void onDraw() {
		float animationDuration = 150f;
		if(isInBounds(Chess.getInstance().mouseX, Chess.getInstance().mouseY) != inBounds) {
			animationTime = Chess.getInstance().millis();
			inBounds = !inBounds;
		}
		
		
		getGraphics().noStroke();
		int millis = Chess.getInstance().millis();
		
		float animation = Math.min((millis - animationTime) / animationDuration, 1f);
		int color = getGraphics().lerpColor(0xFFF0F0F0, 0xFFC8C8C8, inBounds ? animation : 1f - animation);
		getGraphics().fill(color);
		getGraphics().rect(x, y, width, height, height);
		getGraphics().textAlign(PApplet.CENTER, PApplet.CENTER);
		getGraphics().fill(0);
		getGraphics().textSize(fontSize);
		getGraphics().text(text, x + (width / 2), y + (height / 2));
	}
	
	

	public PGraphics getGraphics() {
		return Chess.getInstance().getGraphics();
	}

	@Override
	public void onHover() {
		
	}

}
