package me.benfah.screens.game;

import me.benfah.logic.Vector;
import me.benfah.network.PacketHandler;
import me.benfah.network.packet.GameMovePacket;

public class ChessMultiplayerScreen extends ChessScreen{
	
	boolean playAsWhite;
	
	public ChessMultiplayerScreen(boolean playAsWhite) {
		super();
		this.playAsWhite = playAsWhite;
	}
	
	@Override
	public void onSelectedPieceMove() {
		Vector to = new Vector(hoverX, hoverY);
		Vector from = getSelectedPiece().getPosition();
		game.playPiece(getSelectedPiece(), to);
		GameMovePacket gmp = new GameMovePacket();
		PacketHandler.dispatchPacket(gmp.setFrom(from).setTo(to));
		checkForLoss();
	}

	@Override
	public boolean canSelect() {
		return game.isWhiteTurn() == playAsWhite;
	}
	
	@Override
	public void exitGame() {
		
		super.exitGame();
	}
	
	public void playRemote(Vector from, Vector to) {
		game.playPiece(game.getPiece(from), to);
		checkForLoss();
	}

}
