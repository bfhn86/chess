package me.benfah.utils;


import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.Iterator;

import me.benfah.logic.ChessGame;
import me.benfah.logic.IChessGame;
import me.benfah.logic.PieceMove;
import me.benfah.logic.Serializer;
import me.benfah.logic.Vector;
import me.benfah.logic.piece.IPiece;
import me.benfah.logic.piece.PieceType;

public class Serializers
{
	public static Serializer<IChessGame> CHESS_WITH_HISTORY = new Serializer<>() {

		@Override
		public String serialize(IChessGame t) {
			
			Iterator<PieceMove> it = t.getHistory().descendingIterator();
			
			String history = "";
			
			while(it.hasNext()) {
				PieceMove m = it.next();
				String from = VECTOR.serialize(m.from());
				String to = VECTOR.serialize(m.to());
				String beatPiece = m.beatPiece() != null ? PIECE.serialize(m.beatPiece()) : "null";
				history += from + "|" + to  + "|" + beatPiece + "\n";
			}
			
			String result = history + (t.isWhiteTurn() ? "1 " : "0 ");
			for(int i = 0; i < t.getPieceArray().length; i++) {
				IPiece p = t.getPieceArray()[i];
				if(p == null) {
					result += "null ";
					continue;
				}
				result += PIECE.serialize(p) + " ";
			}
			return result;
		}

		@Override
		public IChessGame deserialize(String s) {
			String[] seperatedArr = s.split("\n");
			String[] history = Arrays.copyOfRange(seperatedArr, 0, seperatedArr.length - 1);
			Deque<PieceMove> historyDeque = new ArrayDeque<>();
			for(String entry : history) {
				String[] entryArr = entry.split("\\|");
				Vector from = VECTOR.deserialize(entryArr[0]);
				Vector to = VECTOR.deserialize(entryArr[1]);
				IPiece beatPiece = !entryArr[2].equals("null") ? PIECE.deserialize(entryArr[2]) : null;
				historyDeque.push(new PieceMove(from, to, beatPiece));
			}
			
			String[] arr = seperatedArr[seperatedArr.length - 1].split(" ");
			IPiece[] pieceArray = new IPiece[8 * 8];
			boolean whiteTurn = arr[0].equals("1");
			for(int i = 1; i < arr.length; i++) {
				String x = arr[i];
				if(x.equals("null")) {
					pieceArray[i] = null;
					continue;
				}
				IPiece p = PIECE.deserialize(x);
				
				pieceArray[i - 1] = p;
			}
			
			IChessGame board = new ChessGame(pieceArray);
			historyDeque.stream().filter(x -> x.beatPiece() != null).forEach(x -> x.beatPiece().setBoard(board));
			board.setHistory(historyDeque);
			if(board.isWhiteTurn() != whiteTurn)
				board.toggleTurn();
			return board;
			
			
			
		}
	
	
	};
	
	public static Serializer<Vector> VECTOR = new Serializer<>() {

		@Override
		public String serialize(Vector t) {
			return t.getX() + " " + t.getY();
		}

		@Override
		public Vector deserialize(String s) {
			String[] arr = s.split(" ");
			return new Vector(Integer.parseInt(arr[0]), Integer.parseInt(arr[1]));
		}
	};
	
	public static Serializer<IPiece> PIECE = new Serializer<IPiece>() {

		@Override
		public String serialize(IPiece t) {
			
			return "" + t.getSymbol() + (t.isWhite() ? "1" : "0") + t.getMoveCount();
		}

		@Override
		public IPiece deserialize(String s) {
			PieceType<?> pt = PieceType.byNotation(s.charAt(0));
			IPiece result = pt.instance(s.charAt(1) == '1', null);
			result.setMoveCount(Integer.parseInt(s.substring(2)));
			return result;
		}
	};
	
}
