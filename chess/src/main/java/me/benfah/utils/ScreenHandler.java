package me.benfah.utils;

import java.io.File;

import me.benfah.Chess;
import me.benfah.screens.Screen;
import me.benfah.screens.MainScreen;

public class ScreenHandler
{
    private static Screen currentScreen = new MainScreen();
    
    public static void onMouseClick() {
    	currentScreen.mouseClicked();
    }
    
    public static void onMouseMove() {
    	currentScreen.mouseMoved();
    }
    
    public static void onDraw() {
    	currentScreen.draw(Chess.getInstance().getGraphics());
    }
	
    public static void setScreen(Screen s) {
    	currentScreen = s;
    }
    
    public static Screen getScreen() {
    	return currentScreen;
    }
    
    public static void handleFile(File f) {
    	currentScreen.handleFile(f);
    }

	public static void onKeyPressed() {
		currentScreen.keyPressed();
	}
    
}
