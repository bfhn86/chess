package me.benfah.utils;

import me.benfah.logic.Vector;
import me.benfah.logic.piece.IPiece;
import me.benfah.screens.game.ChessScreen;
import processing.core.PGraphics;
import processing.core.PShape;

public class PieceRenderer {
	
	PGraphics g;
	
	public PieceRenderer(PGraphics g) {
		this.g = g;
	}
	
	public void renderPiece(IPiece p) {
		Vector pos = p.getPosition();
		PShape shape = ShapeHandler.getShape(p.getGraphic());
		
		g.shape(shape, pos.getX() * ChessScreen.FIELD_SIZE, pos.getY() * ChessScreen.FIELD_SIZE, ChessScreen.FIELD_SIZE, ChessScreen.FIELD_SIZE);

	}
	
}
