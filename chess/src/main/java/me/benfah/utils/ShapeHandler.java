package me.benfah.utils;

import java.util.HashMap;
import java.util.Map;

import me.benfah.Chess;
import processing.core.PShape;

public class ShapeHandler {
	
	
	static Map<String, PShape> shapes = new HashMap<>();
	
	public static PShape getShape(String path) {
		if(shapes.containsKey(path))
			return shapes.get(path);
		
		PShape result = Chess.getInstance().loadShape(path);
		shapes.put(path, result);
		return result;
	}
	
}
