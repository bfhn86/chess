package me.benfah.logic;

import me.benfah.logic.piece.IPiece;

public record PieceMove(Vector from, Vector to, IPiece beatPiece) {}
