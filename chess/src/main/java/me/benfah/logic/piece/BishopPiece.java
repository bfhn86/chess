package me.benfah.logic.piece;

import me.benfah.logic.Directions;
import me.benfah.logic.IChessGame;
import me.benfah.logic.Vector;

public class BishopPiece extends VectorPiece {

	public BishopPiece(boolean isWhite, IChessGame board) {
		super(isWhite, board, 'b');
	}

	@Override
	public Vector[] getDirections() {
		return Directions.BISHOP_DIRECTIONS;
	}

}
