package me.benfah.logic.piece;

import java.util.ArrayList;

import me.benfah.logic.IChessGame;
import me.benfah.logic.Vector;

public class KnightPiece extends Piece {

	public KnightPiece(boolean isWhite, IChessGame board) {
		super(isWhite, board, 'n');
	}

	@Override
	public Vector[] calculateMoves() {
		Vector currentPos = getPosition();
		ArrayList<Vector> result = new ArrayList<>();
		int boardSize = 8;

		for (int i : new int[] { 1, -1 }) {
			for (int j : new int[] { 1, -1 }) {
				for (int[] k : new int[][] { { 2, 1 }, { 1, 2 } }) {
					int x = currentPos.getX() + (i * k[0]);
					int y = currentPos.getY() + (j * k[1]);

					if (x >= 0 && x < boardSize && y >= 0 && y < boardSize) {
						Vector pos = new Vector(x, y);

						if (board.isEmpty(pos) || board.getPiece(pos).isWhite() != isWhite()) {
							result.add(pos);
						}
					}
				}

			}
		}

		return result.toArray(new Vector[0]);
	}

}
