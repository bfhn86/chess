package me.benfah.logic.piece;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Stream;

import me.benfah.logic.IChessGame;
import me.benfah.logic.Vector;

public class KingPiece extends Piece {

	boolean isInCheck = false;

	public KingPiece(boolean isWhite, IChessGame board) {
		super(isWhite, board, 'k');
	}

	@Override
	public Vector[] calculateMoves() {
		Vector currentPos = getPosition();
		ArrayList<Vector> result = new ArrayList<>();
		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				Vector pos = currentPos.add(i, j);
				if (pos.isValid() && !Arrays.stream(board.getCaptureMoves(!isWhite())).anyMatch(x -> x.equals(pos))) {
					if (board.isEmpty(pos) || board.getPiece(pos).isWhite() != isWhite()) {
						result.add(pos);
					}
				}
			}
		}

		return result.toArray(new Vector[0]);
	}

	@Override
	public void onBoardPreUpdate() {

	}

	public boolean isInCheck() {
		return board.getPieces(!isWhite).stream().flatMap(x -> Arrays.stream(x.getCaptureMoves()))
				.anyMatch(x -> x.equals(getPosition()));
	}

	@Override
	public Vector[] checkFilter(Vector[] v) {
		IPiece p = getCheckPiece();
		Vector checkPiecePos = p.getPosition();
		Stream<Vector> stream = Arrays.stream(v)
				.filter(x -> !Arrays.asList(board.getCaptureMoves(!isWhite)).contains(x));
		if (p instanceof VectorPiece)
			stream = stream.filter(x -> !x.sub(checkPiecePos).isCollinear(((VectorPiece) p).checkTrajectory)
					|| x.equals(checkPiecePos));
		return stream.toArray(Vector[]::new);
	}

}
