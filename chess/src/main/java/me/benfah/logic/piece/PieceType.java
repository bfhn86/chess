package me.benfah.logic.piece;

import java.util.Arrays;

import me.benfah.logic.IChessGame;

public class PieceType<T extends IPiece> {

	public static final PieceType<RookPiece> ROOK = new PieceType<>(RookPiece.class, 'r');
	public static final PieceType<PawnPiece> PAWN = new PieceType<>(PawnPiece.class, 'p');
	public static final PieceType<QueenPiece> QUEEN = new PieceType<>(QueenPiece.class, 'q');
	public static final PieceType<KnightPiece> KNIGHT = new PieceType<>(KnightPiece.class, 'n');
	public static final PieceType<KingPiece> KING = new PieceType<>(KingPiece.class, 'k');
	public static final PieceType<BishopPiece> BISHOP = new PieceType<>(BishopPiece.class, 'b');

	public static final PieceType<?>[] ALL_TYPES = new PieceType<?>[] { ROOK, PAWN, QUEEN, KNIGHT, KING, BISHOP };

	Class<T> clazz;
	char notation;

	private PieceType(Class<T> clazz, char notation) {
		this.clazz = clazz;
		this.notation = notation;
	}

	public Class<T> getPieceClass() {
		return clazz;
	}

	public boolean is(IPiece p) {
		return p.getClass().equals(clazz);
	}

	public char getNotation() {
		return notation;
	}

	public static PieceType<?> byNotation(char c) {
		return Arrays.stream(ALL_TYPES).filter(x -> x.getNotation() == c).findFirst().orElse(null);
	}

	public T instance(boolean isWhite, IChessGame board) {
		try {
			return clazz.getConstructor(boolean.class, IChessGame.class).newInstance(isWhite, board);
		} catch (Exception e) {
			return null;
		}
	}

	
}
