package me.benfah.logic.piece;

import java.util.ArrayList;
import java.util.Arrays;

import me.benfah.logic.IChessGame;
import me.benfah.logic.Vector;

public abstract class VectorPiece extends Piece {

	Vector checkTrajectory;

	public VectorPiece(boolean isWhite, IChessGame board, char symbol) {
		super(isWhite, board, symbol);
	}

	public abstract Vector[] getDirections();

	public Vector[] filterCheckTrajectory(Vector[] moves) {
		Vector pos = getPosition();
		Vector kingPos = board.getFirstPiece(PieceType.KING, !isWhite).getPosition();
		return Arrays.stream(moves)
				.filter(x -> x.sub(pos).isPositivelyCollinear(checkTrajectory)
						&& x.sub(pos).getFactorOf(checkTrajectory) < kingPos.sub(pos).getFactorOf(checkTrajectory))
				.toArray(Vector[]::new);
	}

	@Override
	public Vector[] calculateMoves() {
		ArrayList<Vector> result = new ArrayList<>();
		for (Vector v : getDirections()) {
			Vector currentPos = getPosition();
			boolean moveContinue = true;
			boolean protectorContinue = true;
			IPiece protector = null;
			while ((currentPos = currentPos.add(v)).isValid()) {
				if (board.isEmpty(currentPos)) {
					if (moveContinue)
						result.add(currentPos);
				} else if (board.getPiece(currentPos).isWhite() != isWhite()) {
					if (board.getPiece(currentPos) instanceof KingPiece) {
						if (moveContinue)
							checkTrajectory = v;
						if (protector != null && protectorContinue)
							protector.setProtectorDirection(v);
					} else {
						if (protector != null) {
							protectorContinue = false;
						}
						protector = board.getPiece(currentPos);

					}
					if (moveContinue)
						result.add(currentPos);
					moveContinue = false;

				} else {
					result.add(currentPos);
					break;
				}
				

			}
		}

		return result.toArray(new Vector[0]);
	}

}
