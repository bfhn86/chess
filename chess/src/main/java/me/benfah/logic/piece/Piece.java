package me.benfah.logic.piece;

import java.util.Arrays;
import java.util.stream.Stream;

import me.benfah.logic.IChessGame;
import me.benfah.logic.Vector;

public abstract class Piece implements IPiece {

	boolean isWhite;
	IChessGame board;
	char symbol;

	boolean checkUpdate, isInCheck;

	public Vector protectorDirection;
	int moveCount = 0;

	public Piece(boolean isWhite, IChessGame board, char symbol) {
		this.isWhite = isWhite;
		this.board = board;
		this.symbol = symbol;
	}

	/**
	 * Get all allowed capture moves, that are executable by the piece. A capture
	 * move is defined as a move which is capable of capturing a piece.
	 * 
	 * @return A list of positions
	 */
	public Vector[] getAllowedCaptureMoves() {
		return filter(Arrays.stream(calculateMoves()).filter(this::isMoveAllowed).toArray(Vector[]::new));
	}

	/**
	 * Get all allowed moves, that are executable by the piece
	 * 
	 * @return A list of positions
	 */
	public Vector[] getAllowedMoves() {
		return Stream.concat(Arrays.stream(getAllowedCaptureMoves()), Arrays.stream(getAllowedNonCaptureMoves()))
				.toArray(Vector[]::new);
	}

	/**
	 * Get all capture moves, whether they can be executed or not (e.g. pawn piece)
	 * 
	 * @return A list of positions
	 */
	public Vector[] getCaptureMoves() {
		return calculateMoves();
	}

	/**
	 * Get all moves by a piece which are not capable of capturing a piece.
	 * 
	 * @return
	 */
	public Vector[] getAllowedNonCaptureMoves() {
		return filter(calculateNonCaptureMoves());
	}

	public String getGraphic() {
		return "Chess_" + symbol + (isWhite ? "l" : "d") + "t45.svg";
	}

	public char getSymbol() {
		return symbol;
	}

	public void setBoard(IChessGame board) {
		this.board = board;
	}

	public Vector getPosition() {
		return board.getPiecePosition(this);
	}

	public void onMove() {
		moveCount++;
	}

	public abstract Vector[] calculateMoves();

	public boolean isMoveAllowed(Vector v) {
		return board.isEmpty(v) || (board.getPiece(v).isWhite() != isWhite());
	}

	public Vector[] calculateNonCaptureMoves() {
		return new Vector[0];
	}

	public boolean isWhite() {
		return isWhite;
	}

	public void setMoveCount(int i) {
		this.moveCount = i;
	}

	public int getMoveCount() {
		return this.moveCount;
	}

	public IPiece getCheckPiece() {
		Vector currentPos = getPosition();
		return board.getPieces(!isWhite).stream().parallel()
				.filter(x -> Arrays.stream(x.getCaptureMoves()).anyMatch(y -> y.equals(currentPos))).findAny()
				.orElse(null);
	}

	public Vector[] checkFilter(Vector[] v) {
		KingPiece king = board.getFirstPiece(PieceType.KING, isWhite);

		if (king.getCheckPiece() instanceof VectorPiece) {
			return ((VectorPiece) king.getCheckPiece()).filterCheckTrajectory(v);
		} else {
			return Arrays.stream(v).filter(x -> x.equals(king.getCheckPiece().getPosition())).toArray(Vector[]::new);
		}
	}

	public Vector[] protectorFilter(Vector[] v) {
		Vector pos = getPosition();
		return Arrays.stream(v).filter(x -> x.sub(pos).isCollinear(protectorDirection)).toArray(Vector[]::new);
	}

	public void onBoardPreUpdate() {
	}

	public void onBoardUpdate() {
		onPieceUpdate();
		protectorDirection = null;
	}

	public void onPieceUpdate() {

	}

	public Vector[] filter(Vector[] v) {
		if (board.getFirstPiece(PieceType.KING, isWhite).isInCheck() && protectorDirection != null)
			return protectorFilter(checkFilter(v));
		else if (protectorDirection != null)
			return protectorFilter(v);
		else if (board.getFirstPiece(PieceType.KING, isWhite).isInCheck())
			return checkFilter(v);
		return v;
	}

	public void setProtectorDirection(Vector v) {
		this.protectorDirection = v;
		onPieceUpdate();
	}

	public IChessGame getBoard() {
		return board;
	}

	public Vector getProtectorDirection() {
		return protectorDirection;
	}

}
