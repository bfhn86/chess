package me.benfah.network.io;

import java.util.List;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import me.benfah.network.PacketHandler;
import me.benfah.network.packet.Packet;

public class PacketDecoder extends ByteToMessageDecoder {

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
		int id = in.readInt();
		Packet t = PacketHandler.getPacketInstance(id);
		t.read(in);
		out.add(t);
	}

}
