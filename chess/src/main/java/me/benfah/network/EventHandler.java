package me.benfah.network;

import java.util.Collection;
import java.util.function.Consumer;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;

import me.benfah.network.packet.GameMovePacket;
import me.benfah.network.packet.GameStatePacket;
import me.benfah.network.packet.Packet;
import me.benfah.screens.game.ChessMultiplayerScreen;
import me.benfah.utils.ScreenHandler;

public class EventHandler {

	static MultiValuedMap<Class<? extends Packet>, Consumer<Packet>> consumerMap = new HashSetValuedHashMap<>();
	
	static {
		addListener(GameStatePacket.class, EventHandler::onGameState);
		addListener(GameMovePacket.class, EventHandler::onGameMove);

	}
	
	@SuppressWarnings("unchecked")
	public static <T extends Packet> void addListener(Class<T> clazz, Consumer<T> listener) {
		consumerMap.put(clazz, (Consumer<Packet>) listener);
	}
	
	public static Collection<Consumer<Packet>> getListeners(Class<? extends Packet> clazz) {
		return consumerMap.get(clazz);
	}
	
	public static void onGameState(GameStatePacket msg) {
		if(msg.getState() == GameStatePacket.INIT)
			ScreenHandler.setScreen(new ChessMultiplayerScreen(true));
	}
	
	public static void onGameMove(GameMovePacket msg) {
		if(ScreenHandler.getScreen() instanceof ChessMultiplayerScreen) {
			ChessMultiplayerScreen cmps = (ChessMultiplayerScreen) ScreenHandler.getScreen();
			cmps.playRemote(msg.getFrom(), msg.getTo());
		}
	}
}
