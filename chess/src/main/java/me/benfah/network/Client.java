package me.benfah.network;

import java.net.InetSocketAddress;
import java.util.concurrent.ExecutionException;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import me.benfah.network.io.PacketChannelInboundHandler;
import me.benfah.network.io.PacketDecoder;
import me.benfah.network.io.PacketEncoder;
import me.benfah.screens.MainScreen;
import me.benfah.utils.ScreenHandler;

public class Client extends ChannelInitializer<Channel> {

    private final Bootstrap bootstrap;

    private final EventLoopGroup workerGroup = new NioEventLoopGroup();
    
    public static Client currentClient;
    
    public Client(Runnable doneCallback) throws Exception {
        this.bootstrap = new Bootstrap()
                .option(ChannelOption.AUTO_READ, true)
                .option(ChannelOption.SO_KEEPALIVE, true)
                .group(workerGroup)
                .handler(this)
                .channel(NioSocketChannel.class);

        try {
            ChannelFuture cf = this.bootstrap.connect(new InetSocketAddress("127.0.0.1", 6355))
                    .awaitUninterruptibly().sync().addListener((f) -> doneCallback.run());
            if(!cf.isSuccess()) {
            	throw new Exception("Not correctly initialized");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void initChannel(Channel channel) throws Exception {
    	
        channel.pipeline().addLast(new PacketDecoder(), new PacketEncoder(), new PacketChannelInboundHandler());
        channel.closeFuture().addListener(x -> {
        	currentClient = null;
        	ScreenHandler.setScreen(new MainScreen());
        });
        PacketHandler.setChannel(channel);
    }

    public void shutdown() {
        try {
            workerGroup.shutdownGracefully().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
    
    public static Client initClient(Runnable doneCallback) throws Exception {
		return currentClient = new Client(doneCallback);
    }

    public static Client getClient() {
    	return currentClient;
    }
    
    public static void resetClient() {
    	currentClient = null;
    	if(PacketHandler.getChannel() != null)
    		PacketHandler.getChannel().close();
    }
}