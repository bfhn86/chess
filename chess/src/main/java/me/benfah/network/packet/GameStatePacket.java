package me.benfah.network.packet;

import io.netty.buffer.ByteBuf;

public class GameStatePacket extends Packet {

	public static final int INIT = 0;
	
	int state = 0;

	public GameStatePacket() {
	}

	@Override
	public void read(ByteBuf packetBuffer) {
		state = packetBuffer.readInt();
	}

	@Override
	public void write(ByteBuf packetBuffer) {
		packetBuffer.writeInt(state);
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

}