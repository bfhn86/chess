package me.benfah.network.packet;

import io.netty.buffer.ByteBuf;
import me.benfah.logic.Vector;
import me.benfah.utils.CodecUtils;

public class GameMovePacket extends Packet{
	
	Vector from;
	Vector to;
	
	@Override
	public void read(ByteBuf bb) {
		from = CodecUtils.readVector(bb);
		to = CodecUtils.readVector(bb);
		
	}

	@Override
	public void write(ByteBuf bb) {
		CodecUtils.writeVector(from, bb);
		CodecUtils.writeVector(to, bb);

	}

	public Vector getFrom() {
		return from;
	}

	public GameMovePacket setFrom(Vector from) {
		this.from = from;
		return this;
	}

	public Vector getTo() {
		return to;
	}

	public GameMovePacket setTo(Vector to) {
		this.to = to;
		return this;
	}
	
	
	
}
