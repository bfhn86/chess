package me.benfah.network.packet;

import io.netty.buffer.ByteBuf;

public abstract class Packet
{
	
	
	public Packet()
	{
	}
	
	public abstract void read(ByteBuf bb);
	
	public abstract void write(ByteBuf bb);
}
