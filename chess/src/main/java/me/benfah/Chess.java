package me.benfah;

import java.io.File;

import me.benfah.logic.SQLiteHandler;
import me.benfah.utils.ScreenHandler;
import processing.core.PApplet;
import processing.core.PFont;

public class Chess extends PApplet
{
    static Chess instance;
    public static PFont font;
    public static void main(String[] args) {
    	instance = new Chess();
        PApplet.runSketch(new String[]{""}, instance);
    }

    @Override
    public void settings() {
        size(800, 800);
    }
    
    @Override
    public void setup() {
    	SQLiteHandler.init();
        font = loadFont("Playfair.vlw");
        textFont(font);
        
       
        textAlign(CENTER, TOP);

    }
    
    @Override
    public void draw() {
    	
    	ScreenHandler.onDraw();
    }
    
    @Override
    public void mouseClicked() {
    	ScreenHandler.onMouseClick();
    }
    
    @Override
    public void mouseMoved() {
    	ScreenHandler.onMouseMove();
    }
    
    @Override
    public void keyPressed() {
    	ScreenHandler.onKeyPressed();
    }
    
    public void fileSelector(File f) {
    	ScreenHandler.handleFile(f);
    }
    
    public static Chess getInstance() {
    	return instance;
    }
    
}
