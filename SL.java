public class ChessGame implements IChessGame {

	public IPiece[] pieceArray = new IPiece[8 * 8];
	boolean whiteTurn = true;
	Deque<PieceMove> oldMoves = new ArrayDeque<>();

	Vector[] whiteCaptureMoves = new Vector[0];
	Vector[] blackCaptureMoves = new Vector[0];

	Vector[] whiteAllowedCaptureMoves = new Vector[0];
	Vector[] blackAllowedCaptureMoves = new Vector[0];

	int won = -1;

	public ChessGame() {
		setupGame();
	}

	public ChessGame(IPiece[] arr) {
		assert arr != null : "Array isn't allowed to be null";
		this.pieceArray = arr;
		Arrays.stream(pieceArray).filter(x -> x != null).forEach(x -> x.setBoard(this));
		calculateCheckMoves();
	}

	public void setupGame() {
		for (int i = 0; i < 8; i++) {
			setPiece(new Vector(i, 6), new PawnPiece(true, this));
		}

		for (int i = 0; i < 8; i++) {
			setPiece(new Vector(i, 1), new PawnPiece(false, this));
		}

		for (int i : new int[] { 7, 0 }) {
			boolean isWhite = i == 7;
			setPiece(new Vector(0, i), new RookPiece(isWhite, this));
			setPiece(new Vector(7, i), new RookPiece(isWhite, this));

			setPiece(new Vector(1, i), new KnightPiece(isWhite, this));
			setPiece(new Vector(6, i), new KnightPiece(isWhite, this));

			setPiece(new Vector(2, i), new BishopPiece(isWhite, this));
			setPiece(new Vector(5, i), new BishopPiece(isWhite, this));

			setPiece(new Vector(4, i), new QueenPiece(isWhite, this));
			setPiece(new Vector(3, i), new KingPiece(isWhite, this));
		}

		calculateCheckMoves();

	}

	public static int getBoardSize() {
		return 8;
	}

	public boolean undoMove() {
		if (!oldMoves.isEmpty()) {
			PieceMove pm = oldMoves.pop();
			IPiece movedPiece = getPiece(pm.to());
			setPiece(pm.from(), movedPiece);
			setPiece(pm.to(), pm.beatPiece());
			toggleTurn();
			movedPiece.setMoveCount(movedPiece.getMoveCount() - 1);
			onBoardUpdate();
			return true;
		}
		return false;
	}

	public Deque<PieceMove> getHistory() {
		return oldMoves;
	}

	public void setHistory(Deque<PieceMove> history) {
		assert history != null: "History can't be null";
		oldMoves = history;
	}

	public Vector getPiecePosition(IPiece p) {
		assert p != null : "Piece can't be null";
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				Vector res = new Vector(i, j);
				if (p.equals(getPiece(res)))
					return res;
			}
		}
		return null;
	}

	public void movePiece(IPiece p, Vector pos) {
		Vector currentPos = getPiecePosition(p);

		oldMoves.push(new PieceMove(currentPos, pos, getPiece(pos)));
		setPiece(currentPos, null);
		setPiece(pos, p);

	}

	public Vector[] getCaptureMoves(boolean isWhite) {
		return isWhite ? whiteCaptureMoves : blackCaptureMoves;
	}

	public Vector[] getAllowedCaptureMoves(boolean isWhite) {
		return isWhite ? whiteAllowedCaptureMoves : blackAllowedCaptureMoves;
	}

	public boolean isWhiteTurn() {
		return whiteTurn;
	}

	public void toggleTurn() {
		whiteTurn = !whiteTurn;
	}

	private void calculateCheckMoves() {
		whiteCaptureMoves = Arrays.stream(pieceArray).parallel().filter(x -> x != null && x.isWhite())
				.flatMap(x -> Arrays.stream(x.getCaptureMoves())).toArray(Vector[]::new);
		blackCaptureMoves = Arrays.stream(pieceArray).parallel().filter(x -> x != null && !x.isWhite())
				.flatMap(x -> Arrays.stream(x.getCaptureMoves())).toArray(Vector[]::new);

		whiteAllowedCaptureMoves = Arrays.stream(pieceArray).parallel().filter(x -> x != null && x.isWhite())
				.flatMap(x -> Arrays.stream(x.getAllowedCaptureMoves())).toArray(Vector[]::new);
		blackAllowedCaptureMoves = Arrays.stream(pieceArray).parallel().filter(x -> x != null && !x.isWhite())
				.flatMap(x -> Arrays.stream(x.getAllowedCaptureMoves())).toArray(Vector[]::new);

	}

	public void onBoardUpdate() {
		calculateCheckMoves();
		Arrays.stream(pieceArray).filter(x -> x != null).forEach(x -> x.onBoardPreUpdate());
		Arrays.stream(pieceArray).filter(x -> x != null).forEach(x -> x.onBoardUpdate());

		calculateCheckMoves();
	}

	public static int flatCoords(int x, int y) {
		int res = y * 8 + x;
		assert res >= 0 && res < 63 : "Coordinates must be valid";
		return res;
	}

	public static Vector toPosition(int boardPos) {
		return new Vector(boardPos % 8, boardPos / 8);
	}

	@Override
	public IPiece[] getPieceArray() {
		return pieceArray;
	}

	@Override
	public IPiece getPiece(Vector v) {
		return pieceArray[flatCoords(v.getX(), v.getY())];
	}

	@Override
	public void setPiece(Vector v, IPiece p) {
		pieceArray[flatCoords(v.getX(), v.getY())] = p;
	}

}
public class Directions {

	public static final Vector[] BISHOP_DIRECTIONS = new Vector[] { new Vector(1, 1), new Vector(1, -1), new Vector(-1, 1), new Vector(-1, -1) };

	public static final Vector[] ROOK_DIRECTIONS = new Vector[] { new Vector(1, 0), new Vector(-1, 0), new Vector(0, 1), new Vector(0, -1) };

	public static final Vector[] QUEEN_DIRECTIONS = Stream.concat(Arrays.stream(BISHOP_DIRECTIONS), Arrays.stream(ROOK_DIRECTIONS)).toArray(Vector[]::new);
}
public class Heuristic {
	static PieceComparator PIECE_COMPARATOR = new PieceComparator();

	public static int evaluateBoard(IChessGame game) {
		int eval = 0;
		for (boolean isWhite : new boolean[] { true, false }) {
			eval += (isWhite ? 1 : -1) * 2000 * game.getPieces(PieceType.KING, isWhite).size();
			eval += (isWhite ? 1 : -1) * 90 * game.getPieces(PieceType.QUEEN, isWhite).size();
			eval += (isWhite ? 1 : -1) * 50 * game.getPieces(PieceType.ROOK, isWhite).size();
			eval += (isWhite ? 1 : -1) * 30 * game.getPieces(PieceType.BISHOP, isWhite).size();
			eval += (isWhite ? 1 : -1) * 30 * game.getPieces(PieceType.KNIGHT, isWhite).size();
			eval += (isWhite ? 1 : -1) * 10 * game.getPieces(PieceType.PAWN, isWhite).size();
			eval -= (isWhite ? 1 : -1) * 5 * game.getPieces(PieceType.PAWN, isWhite).stream().filter(x -> ((PawnPiece) x).isBlocked()).count();
			eval -= (isWhite ? 1 : -1) * 5 * game.getPieces(PieceType.PAWN, isWhite).stream().filter(x -> ((PawnPiece) x).isDoubled()).count();
			eval -= (isWhite ? 1 : -1) * 5 * game.getPieces(PieceType.PAWN, isWhite).stream().filter(x -> ((PawnPiece) x).isIsolated()).count();
			eval += (isWhite ? 1 : -1) * 1 * game.getPieces(isWhite).stream().flatMap(x -> Arrays.stream(x.getAllowedMoves())).count();
		}
		return eval;
	}

	public static Score getBestMove(IChessGame board) {
		ArrayList<CompletableFuture<Score>> futures = new ArrayList<>();

		for (IPiece p : board.getPieces(board.isWhiteTurn()).stream().sorted(PIECE_COMPARATOR).toList()) {
			futures.add(CompletableFuture.supplyAsync(() -> {
				Vector bestMove = null;
				IChessGame copy = BoardUtils.copyBoard(board);
				IPiece copyPiece = copy.getPiece(p.getPosition());
				int bestScore = 0;
				for (Vector v : copyPiece.getAllowedMoves()) {
					copy.playPiece(copyPiece, v);

					int minimaxScore = minimax(copy, 1, Integer.MIN_VALUE, Integer.MAX_VALUE, true, copy.isWhiteTurn());

					copy.undoMove();
					if (bestMove == null
							|| (board.isWhiteTurn() ? bestScore < minimaxScore : bestScore > minimaxScore)) {
						bestMove = v;
						bestScore = minimaxScore;
					}
				}
				return new Score(p.getPosition(), bestMove, bestScore, 0);
			}));

		}
		try {
			List<Score> scoreList = new ArrayList<>(CompletableFuture.allOf(futures.toArray(new CompletableFuture[0])).thenApply(x -> futures.stream().map(CompletableFuture::join).filter(y -> y.to() != null).toList()).get());
			Collections.sort(scoreList, (s1, s2) -> (board.isWhiteTurn() ? 1 : -1) * Integer.compare(s2.score(), s1.score()));
			return scoreList.get(0);

		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
			return null;

		}

	}

	public static int minimax(IChessGame board, int depth, int alpha, int beta, boolean maximizingPlayer,
			boolean isWhite) {

		if (board.isCheckmate(true))
			return Integer.MAX_VALUE;

		if (board.isCheckmate(false))
			return Integer.MIN_VALUE;

		if (depth == 0) {
			return Heuristic.evaluateBoard(board);
		}

		if (maximizingPlayer == isWhite) {
			int maxEval = Integer.MIN_VALUE;
			for (IPiece p : board.getPieces(maximizingPlayer == isWhite)) {
				for (Vector v : p.getAllowedMoves()) {
					board.playPiece(p, v);
					Entry e = SQLiteHandler.getEntry(board, maximizingPlayer, isWhite, depth);
					int eval;
					if (e != null) {
						eval = e.score();
					} else {
						eval = minimax(board, depth - 1, alpha, beta, false, isWhite);
						SQLiteHandler.insertEntry(board, eval, depth, maximizingPlayer, isWhite);
					}
					board.undoMove();
					maxEval = Math.max(maxEval, eval);
					alpha = Math.max(alpha, eval);
					if (beta <= alpha)
						break;
				}
				if (beta <= alpha)
					break;
			}

			return maxEval;
		} else {
			int minEval = Integer.MAX_VALUE;
			for (IPiece p : board.getPieces(maximizingPlayer == isWhite)) {
				for (Vector v : p.getAllowedMoves()) {
					board.playPiece(p, v);
					Entry e = SQLiteHandler.getEntry(board, maximizingPlayer, isWhite, depth);
					int eval;
					if (e != null) {
						eval = e.score();
					} else {
						eval = minimax(board, depth - 1, alpha, beta, true, isWhite);
						SQLiteHandler.insertEntry(board, eval, depth, maximizingPlayer, isWhite);
					}
					board.undoMove();
					minEval = Math.min(minEval, eval);
					beta = Math.min(beta, eval);
					if (beta <= alpha)
						break;
				}
				if (beta <= alpha)
					break;
			}

			return minEval;
		}
	}

	public static record Score(Vector from, Vector to, int score, int depth, int row) implements Comparable<Score> {

		Score(Vector from, Vector to, int score, int depth) {
			this(from, to, score, depth, 0);
		}

		Score(Vector from, Vector to, int score) {
			this(from, to, score, 0);
		}

		@Override
		public int compareTo(Score o) {
			return Integer.compare(o.score, score);
		}
	}

	static class PieceComparator implements Comparator<IPiece> {
		List<Class<? extends Piece>> ORDER = List.of(QueenPiece.class, RookPiece.class, BishopPiece.class, KnightPiece.class, KingPiece.class, PawnPiece.class);

		@Override
		public int compare(IPiece o1, IPiece o2) {
			return Integer.compare(ORDER.indexOf(o2.getClass()), ORDER.indexOf(o1.getClass()));
		}

	}

}
public interface IChessGame {

	void setupGame();

	boolean undoMove();

	Vector getPiecePosition(IPiece p);

	default boolean isEmpty(Vector v) {
		return getPiece(v) == null;
	}

	IPiece getPiece(Vector v);

	void setPiece(Vector v, IPiece p);

	void movePiece(IPiece p, Vector v);

	IPiece[] getPieceArray();

	boolean isWhiteTurn();

	void toggleTurn();

	void onBoardUpdate();

	Deque<PieceMove> getHistory();

	void setHistory(Deque<PieceMove> history);

	Vector[] getCaptureMoves(boolean isWhite);

	Vector[] getAllowedCaptureMoves(boolean isWhite);

	@SuppressWarnings("unchecked")
	default <T extends IPiece> List<T> getPieces(PieceType<T> pt, boolean isWhite) {
		assert pt != null : "PieceType can't be null";
		return (List<T>) Arrays.stream(getPieceArray()).filter(x -> x != null && pt.is(x) && x.isWhite() == isWhite).toList();
	}

	@SuppressWarnings("unchecked")
	default <T extends IPiece> List<T> getPieces(PieceType<T> pt) {
		assert pt != null : "PieceType can't be null";
		return (List<T>) Arrays.stream(getPieceArray()).filter(x -> x != null && pt.is(x)).toList();
	}

	default <T extends IPiece> T getFirstPiece(PieceType<T> pt, boolean isWhite) {
		return getPieces(pt, isWhite).get(0);
	}

	default List<IPiece> getPieces(boolean isWhite) {
		return Arrays.stream(getPieceArray()).parallel().filter(x -> x != null && x.isWhite() == isWhite).toList();
	}

	default boolean isCheckmate(boolean isWhite) {
		return getFirstPiece(PieceType.KING, isWhite).isInCheck() && getAllowedCaptureMoves(isWhite).length == 0;
	}

	default boolean isRemis(boolean isWhite) {
		return !getFirstPiece(PieceType.KING, isWhite).isInCheck() && getAllowedCaptureMoves(isWhite).length == 0;
	}

	default boolean isGameOver() {
		return isCheckmate(true) || isCheckmate(false) || isRemis(true) || isRemis(false);
	}

	default void playPiece(IPiece selectedPiece, Vector to) {
		assert selectedPiece != null && to != null && !isGameOver();
		assert Arrays.stream(selectedPiece.getAllowedMoves()).anyMatch(x -> x.equals(to));
		selectedPiece.onMove();
		movePiece(selectedPiece, to);
		toggleTurn();
		onBoardUpdate();
	}

	default Score getBestMove() {
		return Heuristic.getBestMove(this);
	}

}
public record PieceMove(Vector from, Vector to, IPiece beatPiece) {}
public interface Serializer<T>
{
	Serializer<IChessGame> CHESS = new Serializer<>() {

		@Override
		public String serialize(IChessGame t) {
			String result = t.isWhiteTurn() ? "1 " : "0 ";
			for(int i = 0; i < t.getPieceArray().length; i++) {
				IPiece p = t.getPieceArray()[i];
				if(p == null) {
					result += "null ";
					continue;
				}
				result += "" + p.getSymbol() + (p.isWhite() ? "1" : "0") + p.getMoveCount() + " ";
			}
			return result;
		}

		@Override
		public ChessGame deserialize(String s) {
			String[] arr = s.split(" ");
			IPiece[] pieceArray = new IPiece[8 * 8];
			boolean whiteTurn = arr[0].equals("1");
			for(int i = 1; i < arr.length; i++) {
				String x = arr[i];
				if(x.equals("null")) {
					pieceArray[i] = null;
					continue;
				}
				PieceType<?> pt = PieceType.byNotation(x.charAt(0));
				IPiece p = pt.instance(x.charAt(1) == '1', null);
				p.setMoveCount(Integer.parseInt(x.substring(2)));
				pieceArray[i - 1] = p;
			}
			
			ChessGame board = new ChessGame(pieceArray);
			if(board.isWhiteTurn() != whiteTurn)
				board.toggleTurn();
			return board;
			
			
			
		}
	
	
	};
	
	public String serialize(T t);
	
	public T deserialize(String s);
	
}
public class SQLiteHandler {
	static Connection c;

	public static void init() {
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:transposition_tables.db");
			String tableCreation = "CREATE TABLE IF NOT EXISTS transposition_table (id INTEGER PRIMARY KEY AUTOINCREMENT, board TEXT, score INTEGER, depth INTEGER, maximizingPlayer INTEGER, isWhite INTEGER)";
			Statement stmt = c.createStatement();

			stmt.executeUpdate(tableCreation);
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static int insertEntry(IChessGame board, int score, int depth, boolean maximizingPlayer, boolean isWhite) {
		String insert = "INSERT INTO transposition_table(board, score, depth, maximizingPlayer, isWhite) VALUES (?,?,?,?,?)";
		try {
			if (c == null || c.isClosed())
				return -1;

			PreparedStatement ps = c.prepareStatement(insert);
			ps.setString(1, Serializer.CHESS.serialize(board));
			ps.setInt(2, score);
			ps.setInt(3, depth);
			ps.setBoolean(4, maximizingPlayer);
			ps.setBoolean(5, isWhite);
			ps.executeUpdate();
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}

	}

	public static Entry getEntry(IChessGame board, boolean maximizingPlayer, boolean isWhite, int depth) {
		String select = "SELECT * FROM transposition_table WHERE board = ? AND maximizingPlayer = ? AND isWhite = ? AND depth >= ? ORDER BY depth DESC";
		try {
			if (c == null || c.isClosed())
				return null;

			PreparedStatement ps = c.prepareStatement(select);
			ps.setString(1, Serializer.CHESS.serialize(board));
			ps.setBoolean(2, maximizingPlayer);
			ps.setBoolean(3, isWhite);
			ps.setInt(4, depth);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				int id = rs.getRow();
				int score = rs.getInt(2);
				int retrievedDepth = rs.getInt(3);
				return new Entry(id, score, retrievedDepth, maximizingPlayer, isWhite);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static int updateEntry(IChessGame board, Entry s) {
		String insert = "UPDATE transposition_table SET score = ?, depth = ? WHERE id = ?";
		try {
			if (c == null || c.isClosed())
				return -1;

			PreparedStatement ps = c.prepareStatement(insert);
			ps.setInt(1, s.score());
			ps.setInt(2, s.depth());
			ps.setInt(3, s.id());
			ps.executeUpdate();
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}

	}

	public static record Entry(int id, int score, int depth, boolean maximizingPlayer, boolean isWhite) {}

}
public class Vector {

	final int x, y;

	public Vector(int x, int y) {

		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public Vector add(int x, int y) {
		return new Vector(this.x + x, this.y + y);
	}

	public Vector add(Vector v) {
		return new Vector(this.x + v.getX(), this.y + v.getY());
	}

	public Vector sub(Vector v) {
		return new Vector(this.x - v.getX(), this.y - v.getY());
	}

	public Vector multiply(int factor) {
		return new Vector(this.x * factor, this.y * factor);
	}

	public boolean isValid() {
		return x >= 0 && x < 8 && y >= 0 && y < 8;
	}

	public boolean isPositivelyCollinear(Vector v) {
		if (x * v.y - y * v.x != 0) {
			return false;
		}
		return (x * v.x >= 0 && y * v.y >= 0);
	}

	public boolean isCollinear(Vector v) {
		return (x * v.y - y * v.x) == 0;
	}

	public double getFactorOf(Vector v) {
		if (v.x == 0 && v.y == 0)
			throw new IllegalArgumentException("Null vector not allowed");

		try {
			return v.x != 0 ? this.x / v.x : this.y / v.y;

		} catch (ArithmeticException e) {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj.getClass() != getClass())
			return false;
		Vector pos = (Vector) obj;
		return pos.getX() == getX() && pos.getY() == getY();
	}

}
public class BishopPiece extends VectorPiece {

	public BishopPiece(boolean isWhite, IChessGame board) {
		super(isWhite, board, 'b');
	}

	@Override
	public Vector[] getDirections() {
		return Directions.BISHOP_DIRECTIONS;
	}

}
public interface IPiece {

	IChessGame getBoard();

	void setBoard(IChessGame board);

	Vector[] getAllowedCaptureMoves();

	Vector[] getAllowedMoves();

	Vector[] getCaptureMoves();

	Vector[] getAllowedNonCaptureMoves();

	String getGraphic();

	char getSymbol();

	void onMove();

	boolean isMoveAllowed(Vector v);

	boolean isWhite();

	void setMoveCount(int i);

	int getMoveCount();

	Vector getProtectorDirection();

	void setProtectorDirection(Vector v);

	void onBoardPreUpdate();

	void onBoardUpdate();

	default Vector getPosition() {
		return getBoard().getPiecePosition(this);
	}

	default IPiece getCheckPiece() {
		Vector currentPos = getPosition();
		return getBoard().getPieces(!isWhite()).stream().parallel()
				.filter(x -> Arrays.stream(x.getCaptureMoves()).anyMatch(y -> y.equals(currentPos))).findAny()
				.orElse(null);
	}

	default Vector[] checkFilter(Vector[] v) {
		KingPiece king = getBoard().getFirstPiece(PieceType.KING, isWhite());

		if (king.getCheckPiece() instanceof VectorPiece) {
			return ((VectorPiece) king.getCheckPiece()).filterCheckTrajectory(v);
		} else {
			return Arrays.stream(v).filter(x -> x.equals(king.getCheckPiece().getPosition())).toArray(Vector[]::new);
		}
	}

	default Vector[] protectorFilter(Vector[] v) {
		Vector pos = getPosition();
		return Arrays.stream(v).filter(x -> x.sub(pos).isCollinear(getProtectorDirection())).toArray(Vector[]::new);
	}

	default Vector[] filter(Vector[] v) {
		if (getBoard().getFirstPiece(PieceType.KING, isWhite()).isInCheck() && getProtectorDirection() != null)
			return protectorFilter(checkFilter(v));
		else if (getProtectorDirection() != null)
			return protectorFilter(v);
		else if (getBoard().getFirstPiece(PieceType.KING, isWhite()).isInCheck())
			return checkFilter(v);
		return v;
	}

}
public class KingPiece extends Piece {

	boolean isInCheck = false;

	public KingPiece(boolean isWhite, IChessGame board) {
		super(isWhite, board, 'k');
	}

	@Override
	public Vector[] calculateMoves() {
		Vector currentPos = getPosition();
		ArrayList<Vector> result = new ArrayList<>();
		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				Vector pos = currentPos.add(i, j);
				if (pos.isValid() && !Arrays.stream(board.getCaptureMoves(!isWhite())).anyMatch(x -> x.equals(pos))) {
					if (board.isEmpty(pos) || board.getPiece(pos).isWhite() != isWhite()) {
						result.add(pos);
					}
				}
			}
		}

		return result.toArray(new Vector[0]);
	}

	@Override
	public void onBoardPreUpdate() {

	}

	public boolean isInCheck() {
		return board.getPieces(!isWhite).stream().flatMap(x -> Arrays.stream(x.getCaptureMoves()))
				.anyMatch(x -> x.equals(getPosition()));
	}

	@Override
	public Vector[] checkFilter(Vector[] v) {
		IPiece p = getCheckPiece();
		Vector checkPiecePos = p.getPosition();
		Stream<Vector> stream = Arrays.stream(v)
				.filter(x -> !Arrays.asList(board.getCaptureMoves(!isWhite)).contains(x));
		if (p instanceof VectorPiece)
			stream = stream.filter(x -> !x.sub(checkPiecePos).isCollinear(((VectorPiece) p).checkTrajectory)
					|| x.equals(checkPiecePos));
		return stream.toArray(Vector[]::new);
	}

}
public class KnightPiece extends Piece {

	public KnightPiece(boolean isWhite, IChessGame board) {
		super(isWhite, board, 'n');
	}

	@Override
	public Vector[] calculateMoves() {
		Vector currentPos = getPosition();
		ArrayList<Vector> result = new ArrayList<>();
		int boardSize = 8;

		for (int i : new int[] { 1, -1 }) {
			for (int j : new int[] { 1, -1 }) {
				for (int[] k : new int[][] { { 2, 1 }, { 1, 2 } }) {
					int x = currentPos.getX() + (i * k[0]);
					int y = currentPos.getY() + (j * k[1]);

					if (x >= 0 && x < boardSize && y >= 0 && y < boardSize) {
						Vector pos = new Vector(x, y);

						if (board.isEmpty(pos) || board.getPiece(pos).isWhite() != isWhite()) {
							result.add(pos);
						}
					}
				}

			}
		}

		return result.toArray(new Vector[0]);
	}

}
public class PawnPiece extends Piece {

	Vector[] captureMoves = new Vector[0];

	public PawnPiece(boolean isWhite, IChessGame board) {
		super(isWhite, board, 'p');
	}

	@Override
	public Vector[] calculateMoves() {

		List<Vector> result = new ArrayList<>();

		Vector currentPos = getPosition();
		int prefactor = isWhite ? -1 : 1;

		for (int i : new int[] { 1, -1 }) {
			Vector side = currentPos.add(i, prefactor);
			if (side.isValid())
				result.add(side);
		}

		return result.toArray(new Vector[0]);
	}

	@Override
	public Vector[] calculateNonCaptureMoves() {
		List<Vector> result = new ArrayList<>();

		int prefactor = isWhite ? -1 : 1;

		Vector forward = getPosition().add(0, prefactor);
		if (forward.isValid() && board.isEmpty(forward)) {
			result.add(forward);

			Vector forwardSecond = forward.add(0, prefactor);

			if (moveCount == 0 && forwardSecond.isValid() && board.isEmpty(forwardSecond)) {
				result.add(forwardSecond);
			}
		}

		return result.toArray(new Vector[0]);
	}

	public Vector[] calculateCaptureMoves() {

		List<Vector> result = new ArrayList<>();

		Vector currentPos = getPosition();
		int prefactor = isWhite ? -1 : 1;

		for (int i : new int[] { 1, -1 }) {
			Vector side = currentPos.add(i, prefactor);
			if (side.isValid())
				result.add(side);
		}

		return result.toArray(new Vector[0]);
	}

	public boolean isIsolated() {
		boolean result = true;
		Vector pos = getPosition();
		for (int i = 0; i < 8; i++)
			for (int j : new int[] { 1, -1 }) {
				Vector v = new Vector(pos.getX() + j, i);
				if (v.isValid())
					result &= !(board.getPiece(v) instanceof PawnPiece);
			}
		return result;
	}

	public boolean isDoubled() {
		Vector[] positions = new Vector[] { getPosition().add(0, 1), getPosition().add(0, -1) };
		return Arrays.stream(positions).anyMatch(x -> x.isValid() && board.getPiece(x) instanceof PawnPiece);
	}

	public boolean isBlocked() {
		Vector pos = getPosition().add(0, isWhite ? -1 : 1);
		return pos.isValid() && !board.isEmpty(pos);
	}

	@Override
	public boolean isMoveAllowed(Vector v) {
		return !board.isEmpty(v) && board.getPiece(v).isWhite() != isWhite();
	}

}
public abstract class Piece implements IPiece {

	boolean isWhite;
	IChessGame board;
	char symbol;

	boolean checkUpdate, isInCheck;

	public Vector protectorDirection;
	int moveCount = 0;

	public Piece(boolean isWhite, IChessGame board, char symbol) {
		this.isWhite = isWhite;
		this.board = board;
		this.symbol = symbol;
	}

	/**
	 * Get all allowed capture moves, that are executable by the piece. A capture
	 * move is defined as a move which is capable of capturing a piece.
	 * 
	 * @return A list of positions
	 */
	public Vector[] getAllowedCaptureMoves() {
		return filter(Arrays.stream(calculateMoves()).filter(this::isMoveAllowed).toArray(Vector[]::new));
	}

	/**
	 * Get all allowed moves, that are executable by the piece
	 * 
	 * @return A list of positions
	 */
	public Vector[] getAllowedMoves() {
		return Stream.concat(Arrays.stream(getAllowedCaptureMoves()), Arrays.stream(getAllowedNonCaptureMoves()))
				.toArray(Vector[]::new);
	}

	/**
	 * Get all capture moves, whether they can be executed or not (e.g. pawn piece)
	 * 
	 * @return A list of positions
	 */
	public Vector[] getCaptureMoves() {
		return calculateMoves();
	}

	/**
	 * Get all moves by a piece which are not capable of capturing a piece.
	 * 
	 * @return
	 */
	public Vector[] getAllowedNonCaptureMoves() {
		return filter(calculateNonCaptureMoves());
	}

	public String getGraphic() {
		return "Chess_" + symbol + (isWhite ? "l" : "d") + "t45.svg";
	}

	public char getSymbol() {
		return symbol;
	}

	public void setBoard(IChessGame board) {
		this.board = board;
	}

	public Vector getPosition() {
		return board.getPiecePosition(this);
	}

	public void onMove() {
		moveCount++;
	}

	public abstract Vector[] calculateMoves();

	public boolean isMoveAllowed(Vector v) {
		return board.isEmpty(v) || (board.getPiece(v).isWhite() != isWhite());
	}

	public Vector[] calculateNonCaptureMoves() {
		return new Vector[0];
	}

	public boolean isWhite() {
		return isWhite;
	}

	public void setMoveCount(int i) {
		this.moveCount = i;
	}

	public int getMoveCount() {
		return this.moveCount;
	}

	public IPiece getCheckPiece() {
		Vector currentPos = getPosition();
		return board.getPieces(!isWhite).stream().parallel()
				.filter(x -> Arrays.stream(x.getCaptureMoves()).anyMatch(y -> y.equals(currentPos))).findAny()
				.orElse(null);
	}

	public Vector[] checkFilter(Vector[] v) {
		KingPiece king = board.getFirstPiece(PieceType.KING, isWhite);

		if (king.getCheckPiece() instanceof VectorPiece) {
			return ((VectorPiece) king.getCheckPiece()).filterCheckTrajectory(v);
		} else {
			return Arrays.stream(v).filter(x -> x.equals(king.getCheckPiece().getPosition())).toArray(Vector[]::new);
		}
	}

	public Vector[] protectorFilter(Vector[] v) {
		Vector pos = getPosition();
		return Arrays.stream(v).filter(x -> x.sub(pos).isCollinear(protectorDirection)).toArray(Vector[]::new);
	}

	public void onBoardPreUpdate() {
	}

	public void onBoardUpdate() {
		onPieceUpdate();
		protectorDirection = null;
	}

	public void onPieceUpdate() {

	}

	public Vector[] filter(Vector[] v) {
		if (board.getFirstPiece(PieceType.KING, isWhite).isInCheck() && protectorDirection != null)
			return protectorFilter(checkFilter(v));
		else if (protectorDirection != null)
			return protectorFilter(v);
		else if (board.getFirstPiece(PieceType.KING, isWhite).isInCheck())
			return checkFilter(v);
		return v;
	}

	public void setProtectorDirection(Vector v) {
		this.protectorDirection = v;
		onPieceUpdate();
	}

	public IChessGame getBoard() {
		return board;
	}

	public Vector getProtectorDirection() {
		return protectorDirection;
	}

}
public class PieceType<T extends IPiece> {

	public static final PieceType<RookPiece> ROOK = new PieceType<>(RookPiece.class, 'r');
	public static final PieceType<PawnPiece> PAWN = new PieceType<>(PawnPiece.class, 'p');
	public static final PieceType<QueenPiece> QUEEN = new PieceType<>(QueenPiece.class, 'q');
	public static final PieceType<KnightPiece> KNIGHT = new PieceType<>(KnightPiece.class, 'n');
	public static final PieceType<KingPiece> KING = new PieceType<>(KingPiece.class, 'k');
	public static final PieceType<BishopPiece> BISHOP = new PieceType<>(BishopPiece.class, 'b');

	public static final PieceType<?>[] ALL_TYPES = new PieceType<?>[] { ROOK, PAWN, QUEEN, KNIGHT, KING, BISHOP };

	Class<T> clazz;
	char notation;

	private PieceType(Class<T> clazz, char notation) {
		this.clazz = clazz;
		this.notation = notation;
	}

	public Class<T> getPieceClass() {
		return clazz;
	}

	public boolean is(IPiece p) {
		return p.getClass().equals(clazz);
	}

	public char getNotation() {
		return notation;
	}

	public static PieceType<?> byNotation(char c) {
		return Arrays.stream(ALL_TYPES).filter(x -> x.getNotation() == c).findFirst().orElse(null);
	}

	public T instance(boolean isWhite, IChessGame board) {
		try {
			return clazz.getConstructor(boolean.class, IChessGame.class).newInstance(isWhite, board);
		} catch (Exception e) {
			return null;
		}
	}

	
}
public class QueenPiece extends VectorPiece {

	public QueenPiece(boolean isWhite, IChessGame board) {
		super(isWhite, board, 'q');
	}

	@Override
	public Vector[] getDirections() {
		return Directions.QUEEN_DIRECTIONS;
	}

}
public class RookPiece extends VectorPiece {

	public RookPiece(boolean isWhite, IChessGame board) {
		super(isWhite, board, 'r');
	}

	@Override
	public Vector[] getDirections() {
		return Directions.ROOK_DIRECTIONS;
	}

}
public abstract class VectorPiece extends Piece {

	Vector checkTrajectory;

	public VectorPiece(boolean isWhite, IChessGame board, char symbol) {
		super(isWhite, board, symbol);
	}

	public abstract Vector[] getDirections();

	public Vector[] filterCheckTrajectory(Vector[] moves) {
		Vector pos = getPosition();
		Vector kingPos = board.getFirstPiece(PieceType.KING, !isWhite).getPosition();
		return Arrays.stream(moves)
				.filter(x -> x.sub(pos).isPositivelyCollinear(checkTrajectory)
						&& x.sub(pos).getFactorOf(checkTrajectory) < kingPos.sub(pos).getFactorOf(checkTrajectory))
				.toArray(Vector[]::new);
	}

	@Override
	public Vector[] calculateMoves() {
		ArrayList<Vector> result = new ArrayList<>();
		for (Vector v : getDirections()) {
			Vector currentPos = getPosition();
			boolean moveContinue = true;
			boolean protectorContinue = true;
			IPiece protector = null;
			while ((currentPos = currentPos.add(v)).isValid()) {
				if (board.isEmpty(currentPos)) {
					if (moveContinue)
						result.add(currentPos);
				} else if (board.getPiece(currentPos).isWhite() != isWhite()) {
					if (board.getPiece(currentPos) instanceof KingPiece) {
						if (moveContinue)
							checkTrajectory = v;
						if (protector != null && protectorContinue)
							protector.setProtectorDirection(v);
					} else {
						if (protector != null) {
							protectorContinue = false;
						}
						protector = board.getPiece(currentPos);

					}
					if (moveContinue)
						result.add(currentPos);
					moveContinue = false;

				} else {
					result.add(currentPos);
					break;
				}
				

			}
		}

		return result.toArray(new Vector[0]);
	}

}