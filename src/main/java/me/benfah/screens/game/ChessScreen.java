package me.benfah.screens.game;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

import me.benfah.Chess;
import me.benfah.logic.ChessGame;
import me.benfah.logic.IChessGame;
import me.benfah.logic.Vector;
import me.benfah.logic.piece.IPiece;
import me.benfah.screens.EndScreen;
import me.benfah.screens.MainScreen;
import me.benfah.screens.SaveAsScreen;
import me.benfah.screens.Screen;
import me.benfah.screens.components.Button;
import me.benfah.utils.PieceRenderer;
import me.benfah.utils.ScreenHandler;
import me.benfah.utils.Serializers;
import processing.core.PApplet;
import processing.core.PGraphics;

public abstract class ChessScreen extends Screen {
	public static float FIELD_SIZE = 100;
	
	
	IChessGame game;
	Button save, exit;
	int selectedX = -1, selectedY = -1;
	int hoverX = -1, hoverY = -1;
	PieceRenderer renderer;

	public ChessScreen() {
		this(new ChessGame());
	}
	
	public ChessScreen(IChessGame board) {
		game = board;
		save = new Button(0, 0, 50, 50, "S", this::saveGame);
		exit = new Button(0, getHeight() - 50, 50, 50, "E", this::exitGame);

		renderer = new PieceRenderer(getGraphics());
		addInteractable(save);
		addInteractable(exit);

	}
	
	@Override
	public void draw(PGraphics g) {
		
		getGraphics().background(0xFFFFFFFF);
		getGraphics().textSize(40);

		for(int i = 0; i < 8; i++) {
			for(int j = 0; j < 8; j++) {
				if((i + j) % 2 == 0)
					getGraphics().fill(0xFFB88B4A);
				else
					getGraphics().fill(0xFFE3C16F);
				getGraphics().rect(j * FIELD_SIZE, i * FIELD_SIZE, FIELD_SIZE, FIELD_SIZE);
			}
		}
		
		for(int i = 0; i < 8; i++) {
			for(int j = 0; j < 8; j++) {
				Vector vec = new Vector(i, j);
				if(game.getPiece(vec)!= null)
					renderer.renderPiece(game.getPiece(vec));
			}
		}
		if(hoverX != -1 && hoverY != -1 && !game.isEmpty(new Vector(hoverX, hoverY))) {
			g.fill(0);
			g.textAlign(PApplet.TOP, PApplet.TOP);
			g.text("H", hoverX * ChessScreen.FIELD_SIZE, hoverY * ChessScreen.FIELD_SIZE);
		}
		if(selectedX != -1 && selectedY != -1) {
			g.fill(0);
			g.textAlign(PApplet.RIGHT, PApplet.TOP);
			g.text("S", (selectedX + 1) * ChessScreen.FIELD_SIZE, (selectedY) * ChessScreen.FIELD_SIZE);
		}
		if(getSelectedPiece() != null) {
			IPiece p = getSelectedPiece();
			for(Vector pos : p.getAllowedMoves()) {
				g.fill(0x5070c934);
				g.rect(pos.getX() * ChessScreen.FIELD_SIZE, pos.getY() * ChessScreen.FIELD_SIZE, ChessScreen.FIELD_SIZE, ChessScreen.FIELD_SIZE);
			}
		}
				
		super.draw(g);
	}
	
	public abstract boolean canSelect();
	
	public void exitGame() {
		ScreenHandler.setScreen(new MainScreen());
	}
	
	public IPiece getSelectedPiece() {
		if(selectedX != -1 && selectedY != -1)
			return game.getPiece(new Vector(selectedX, selectedY));
		return null;
	}

	public abstract void onSelectedPieceMove();
	
	@Override
	public void mouseClicked() {
		if(canSelect()) {
			Vector hoverVector = new Vector(hoverX, hoverY);
			if(selectedX != -1 && selectedY != -1) {
				if(Arrays.stream(getSelectedPiece().getAllowedMoves()).anyMatch(pos -> pos.getX() == hoverX && pos.getY() == hoverY)) {
					onSelectedPieceMove();
				}
				selectedX = -1;
				selectedY = -1;
			}
			else if(!game.isEmpty(hoverVector) && game.getPiece(hoverVector).isWhite() == game.isWhiteTurn()) {
				selectedX = hoverX;
				selectedY = hoverY;
			}
		} else {
			selectedX = -1;
			selectedY = -1;
		}
		
		super.mouseClicked();
	}
	
	public void checkForLoss() {
		for(boolean b : new boolean[] {true, false}) {
			if(game.isRemis(b))
				ScreenHandler.setScreen(new EndScreen("Remis", (b ? "White" : "Black") + " has no moves, but isn't in check"));
			if(game.isCheckmate(b))
				ScreenHandler.setScreen(new EndScreen("Checkmate", (!b ? "White" : "Black") + " wins"));
		}
	}
	
	public void saveGame() {
		

		if(Chess.getInstance().key == '\uffff') {
			ScreenHandler.setScreen(new SaveAsScreen(game));
		}
		String filename = "game-" + System.currentTimeMillis() + ".bcg";
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(new File(filename)));
			bw.write(Serializers.CHESS_WITH_HISTORY.serialize(game));
			bw.close();
		} catch (IOException e) {
		}
	}
	
	@Override
	public void mouseMoved() {
		hoverX = Chess.getInstance().mouseX / (int) ChessScreen.FIELD_SIZE;
		hoverY = Chess.getInstance().mouseY / (int) ChessScreen.FIELD_SIZE;
		super.mouseMoved();
	}
}
