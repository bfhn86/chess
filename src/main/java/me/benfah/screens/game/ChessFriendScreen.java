package me.benfah.screens.game;


import me.benfah.logic.Vector;
import me.benfah.screens.components.Button;

public class ChessFriendScreen extends ChessScreen{
	
	Button undo;
	
	public ChessFriendScreen() {
		undo = new Button(750, 0, 50, 50, "U", game::undoMove);
		addInteractable(undo);
	}
	
	public void onSelectedPieceMove() {
		game.playPiece(getSelectedPiece(), new Vector(hoverX, hoverY));
		checkForLoss();
	}

	@Override
	public boolean canSelect() {
		return true;
	}
}
