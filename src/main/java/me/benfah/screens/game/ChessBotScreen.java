package me.benfah.screens.game;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import me.benfah.logic.ChessGame;
import me.benfah.logic.Heuristic;
import me.benfah.logic.IChessGame;
import me.benfah.logic.Vector;
import me.benfah.logic.Heuristic.Score;
import me.benfah.screens.components.Button;
import processing.core.PGraphics;

public class ChessBotScreen extends ChessScreen {
	public static float FIELD_SIZE = 100;
	
	CompletableFuture<Score> bestMove;
	
	Button undo;
	boolean playAsWhite;
	public ChessBotScreen(boolean playAsWhite) {
		this(new ChessGame());
		this.playAsWhite = playAsWhite;
		if(!playAsWhite)
			calculateBotMove();
	}
	
	public ChessBotScreen(IChessGame board) {
		super(board);
		undo = new Button(750, 0, 50, 50, "U", this::undoMove);
		addInteractable(undo);
	}
	
	public void undoMove() {
		if(bestMove != null) {
			bestMove.cancel(true);
			bestMove = null;
		}
		game.undoMove();
	}
	
	public void onSelectedPieceMove() {
		game.playPiece(getSelectedPiece(), new Vector(hoverX, hoverY));
		checkForLoss();
		calculateBotMove();
	}
	
	public void calculateBotMove() {
		bestMove = CompletableFuture.supplyAsync(() -> Heuristic.getBestMove(game));
	}
	
	public void playBotMove() {
		try {
			Score s = bestMove.get();
			if(s.to() != null)
				game.playPiece(game.getPiece(s.from()), s.to());
			bestMove = null;
			checkForLoss();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean canSelect() {
		return game.isWhiteTurn() == playAsWhite;
	}
	
	@Override
	public void draw(PGraphics g) {
		super.draw(g);
		if(game.isWhiteTurn() != playAsWhite)
			if(bestMove == null || !bestMove.isDone()) {
				g.fill(255, 255, 255);
				g.textSize(50);
				g.text("Waiting for bot...", getWidth() / 2, 25);
			} else {
				playBotMove();
			}
	}
}
