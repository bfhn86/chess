package me.benfah.screens.network;

import me.benfah.network.Server;
import me.benfah.screens.MainScreen;
import me.benfah.screens.Screen;
import me.benfah.utils.ScreenHandler;
import processing.core.PGraphics;

public class HostScreen extends Screen
{
	
	
	public HostScreen() {

		try {
			Server.initOrGetServer();
		} catch (Exception e) {
			ScreenHandler.setScreen(new MainScreen());
		}
	}
	
	
	@Override
	public void draw(PGraphics g) {
		
		getGraphics().background(256, 256, 256);
		g.textSize(100);
		g.fill(0);
		g.text("Host", getWidth() / 2, 80);
		
		g.textSize(50);
		g.text("Waiting for connection...", getWidth() / 2, getHeight() / 2);
		
		
		super.draw(g);
	}
	
	
	
	
}
