package me.benfah.screens.network;

import me.benfah.Chess;
import me.benfah.network.Client;
import me.benfah.network.PacketHandler;
import me.benfah.network.packet.GameStatePacket;
import me.benfah.screens.Screen;
import me.benfah.screens.game.ChessMultiplayerScreen;
import me.benfah.utils.ScreenHandler;
import processing.core.PGraphics;

public class JoinScreen extends Screen
{
	
	String inputText = "";
	boolean connecting = false;
	
	public JoinScreen() {
	}
	
	@Override
	public void draw(PGraphics g) {
		getGraphics().background(256, 256, 256);
		
		if(inputText.isEmpty()) {

		}
		
		g.fill(0);
		g.textSize(50);
		if(!connecting)
			g.text(inputText.isEmpty() ? "Enter IP..." : inputText, getWidth() / 2, getHeight() / 2);
		else
			g.text("Connecting...", getWidth() / 2, getHeight() / 2);

		super.draw(g);
	}
	
	@Override
	public void keyPressed() {
		if(!connecting) {
			char key = Chess.getInstance().key;
			if(key == '\n') {
				connecting = true;
				try {
					initClient();
				} catch (Exception e) {
					inputText = "";
				}
			} else if(key == '\u0008' && inputText.length() > 0) {
				inputText = inputText.substring(0, inputText.length() - 1);
			} else if(Chess.font.getGlyph(Chess.getInstance().key) != null) {
				inputText += Chess.getInstance().key;

			}
		}
	}
	
	public void initCallback() {
		ScreenHandler.setScreen(new ChessMultiplayerScreen(false));
		GameStatePacket gsp = new GameStatePacket();
		gsp.setState(GameStatePacket.INIT);
		PacketHandler.dispatchPacket(gsp);
	}
	
	public void initClient() throws Exception {
		Client.initClient(this::initCallback);
	}
}
