package me.benfah.screens.components;

public interface IComponent
{
	
	
	boolean isInBounds(int x, int y);
	
	void onClick();
	
	void onDraw();
	
	void onHover();
	
}
