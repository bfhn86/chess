package me.benfah.screens;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import me.benfah.Chess;
import me.benfah.logic.IChessGame;
import me.benfah.network.Server;
import me.benfah.screens.components.Button;
import me.benfah.screens.game.ChessBotScreen;
import me.benfah.screens.game.ChessFriendScreen;
import me.benfah.screens.network.HostScreen;
import me.benfah.screens.network.JoinScreen;
import me.benfah.utils.ScreenHandler;
import me.benfah.utils.Serializers;
import processing.core.PGraphics;

public class MainScreen extends Screen {
	static int BUTTON_WIDTH = 200;
	static int BUTTON_HEIGHT = 50;
	Button playAsWhite;
	Button playAsBlack;

	Button load, host, join, playAgainstFriend;

	public MainScreen() {
		
		playAsWhite = new Button((getWidth() / 2) - BUTTON_WIDTH - 10, 200, 200, 50, "Play as White", 30, () -> onPlayClick(true));
		playAsBlack = new Button(getWidth() / 2 + 10, 200, 200, 50, "Play as Black", 30,  () -> onPlayClick(false));
		playAgainstFriend = new Button(getWidth() / 2 - BUTTON_WIDTH / 2, 300, 200, 50, "Play against Friend", 20, this::onPlayAgainstFriend);

		load = new Button(getWidth() / 2 - BUTTON_WIDTH / 2, 400, 200, 50, "Load", this::onLoad);
		host = new Button(getWidth() / 2 - BUTTON_WIDTH / 2, 500, 200, 50, "Host", this::onHost);
		join = new Button(getWidth() / 2 - BUTTON_WIDTH / 2, 600, 200, 50, "Join", this::onJoin);

		addInteractable(playAsWhite);
		addInteractable(playAsBlack);
		addInteractable(playAgainstFriend);

		addInteractable(load);
		addInteractable(host);
		addInteractable(join);

	}
	
	@Override
	public void draw(PGraphics g) {
		getGraphics().background(256, 256, 256);
		g.textSize(100);
		g.fill(0);
		g.text("Chess", getWidth() / 2, 80);
		super.draw(g);

	}
	
	public void onPlayClick(boolean playAsWhite) {
		ScreenHandler.setScreen(new ChessBotScreen(playAsWhite));
	}
	
	public void onLoad() {
		Chess.getInstance().selectInput("Select game file", "fileSelector");
	}
	
	public void onHost() {
		try {
			Server.initOrGetServer();
			ScreenHandler.setScreen(new HostScreen());
		} catch (Exception e) {
		}
	}
	
	public void onJoin() {
		ScreenHandler.setScreen(new JoinScreen());
	}
	
	public void onPlayAgainstFriend() {
		ScreenHandler.setScreen(new ChessFriendScreen());
	}
	
	@Override
	public void handleFile(File f) {
		if(f == null || !f.exists() || !f.getName().endsWith(".bcg"))
			return;
		
		try {
			
			IChessGame game = Serializers.CHESS_WITH_HISTORY.deserialize(Files.readString(f.toPath()));
			ScreenHandler.setScreen(new ChessBotScreen(game));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
