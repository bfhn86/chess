package me.benfah.network;

import java.util.HashMap;

import io.netty.channel.Channel;
import me.benfah.network.packet.GameMovePacket;
import me.benfah.network.packet.GameStatePacket;
import me.benfah.network.packet.Packet;

public class PacketHandler
{
	static HashMap<Integer, Class<? extends Packet>> map = new HashMap<>();
	
	static Channel channel;

	static {
		registerPacket(1, GameStatePacket.class);
		registerPacket(2, GameMovePacket.class);

	}
	
	public static void registerPacket(int id, Class<? extends Packet> clazz) {
		map.put(id, clazz);
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends Packet> T getPacketInstance(int id) {
		Class<T> clazz = (Class<T>) map.get(id);
		try {
			return clazz.getConstructor().newInstance();
		} catch (ReflectiveOperationException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static int getId(Packet p) {
		return map.entrySet().stream().filter(x -> x.getValue().equals(p.getClass())).findFirst().orElseThrow().getKey();
	}
	
	public static Channel getChannel() {
		return channel;
	}

	public static void setChannel(Channel channel) {
		PacketHandler.channel = channel;
	}
	
	public static void dispatchPacket(Packet p) {
		channel.writeAndFlush(p);
	}
	
}
