package me.benfah.network;

import java.net.InetSocketAddress;
import java.util.concurrent.ExecutionException;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import me.benfah.network.io.PacketChannelInboundHandler;
import me.benfah.network.io.PacketDecoder;
import me.benfah.network.io.PacketEncoder;
import me.benfah.screens.MainScreen;
import me.benfah.utils.ScreenHandler;

public class Server extends ChannelInitializer<Channel> {

    private final ServerBootstrap bootstrap;


    private EventLoopGroup parentGroup = new NioEventLoopGroup();
    private EventLoopGroup workerGroup = new NioEventLoopGroup();

    
    public static Server currentServer;
    
    public Server() throws Exception {
    	
        this.bootstrap = new ServerBootstrap()
                .option(ChannelOption.AUTO_READ, true)
                .childOption(ChannelOption.SO_KEEPALIVE, true)
                .group(parentGroup, workerGroup)
                .childHandler(this)
                .channel(NioServerSocketChannel.class);

        try {
            ChannelFuture cf = this.bootstrap.bind(new InetSocketAddress("0.0.0.0", 6355))
                    .awaitUninterruptibly().sync();
            if(!cf.isSuccess())
            	throw new Exception();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void initChannel(Channel channel) throws Exception {
        channel.pipeline().addLast(new PacketDecoder(), new PacketEncoder(), new PacketChannelInboundHandler());
        channel.closeFuture().addListener(x -> {
        	currentServer.shutdown();
        	currentServer = null;
        	ScreenHandler.setScreen(new MainScreen());
        });
        PacketHandler.setChannel(channel);
    }

    public void shutdown() {
        try {
            parentGroup.shutdownGracefully().get();
            workerGroup.shutdownGracefully().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
    
    public static Server getServer() {
    	return currentServer;
    }
    
    public static Server initOrGetServer() throws Exception {
    	if(currentServer == null) {
    		currentServer = new Server();
    	}
    	return currentServer;
    }
    
    public static void resetServer() {
    	currentServer = null;
    	if(PacketHandler.getChannel() != null)
    		PacketHandler.getChannel().close();
    }
    
}