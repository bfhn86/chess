package me.benfah.network.io;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import me.benfah.network.EventHandler;
import me.benfah.network.packet.Packet;

public class PacketChannelInboundHandler extends SimpleChannelInboundHandler<Packet>{

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, Packet msg) throws Exception {
		EventHandler.getListeners(msg.getClass()).forEach(x -> x.accept(msg));
	}
	
	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		System.out.println("Closed!");
		super.channelInactive(ctx);
	}

}
