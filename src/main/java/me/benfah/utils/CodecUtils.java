package me.benfah.utils;

import io.netty.buffer.ByteBuf;
import me.benfah.logic.Vector;

public class CodecUtils {
	
	public static void writeVector(Vector v, ByteBuf bb) {
		bb.writeInt(v.getX());
		bb.writeInt(v.getY());
	}
	
	public static Vector readVector(ByteBuf bb) {
		return new Vector(bb.readInt(), bb.readInt());
	}
	
}
