package me.benfah.logic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import me.benfah.logic.SQLiteHandler.Entry;
import me.benfah.logic.piece.BishopPiece;
import me.benfah.logic.piece.IPiece;
import me.benfah.logic.piece.KingPiece;
import me.benfah.logic.piece.KnightPiece;
import me.benfah.logic.piece.PawnPiece;
import me.benfah.logic.piece.Piece;
import me.benfah.logic.piece.PieceType;
import me.benfah.logic.piece.QueenPiece;
import me.benfah.logic.piece.RookPiece;

public class Heuristic {
	static PieceComparator PIECE_COMPARATOR = new PieceComparator();

	public static int evaluateBoard(IChessGame game) {
		int eval = 0;
		for (boolean isWhite : new boolean[] { true, false }) {
			eval += (isWhite ? 1 : -1) * 2000 * game.getPieces(PieceType.KING, isWhite).size();
			eval += (isWhite ? 1 : -1) * 90 * game.getPieces(PieceType.QUEEN, isWhite).size();
			eval += (isWhite ? 1 : -1) * 50 * game.getPieces(PieceType.ROOK, isWhite).size();
			eval += (isWhite ? 1 : -1) * 30 * game.getPieces(PieceType.BISHOP, isWhite).size();
			eval += (isWhite ? 1 : -1) * 30 * game.getPieces(PieceType.KNIGHT, isWhite).size();
			eval += (isWhite ? 1 : -1) * 10 * game.getPieces(PieceType.PAWN, isWhite).size();
			eval -= (isWhite ? 1 : -1) * 5 * game.getPieces(PieceType.PAWN, isWhite).stream().filter(x -> ((PawnPiece) x).isBlocked()).count();
			eval -= (isWhite ? 1 : -1) * 5 * game.getPieces(PieceType.PAWN, isWhite).stream().filter(x -> ((PawnPiece) x).isDoubled()).count();
			eval -= (isWhite ? 1 : -1) * 5 * game.getPieces(PieceType.PAWN, isWhite).stream().filter(x -> ((PawnPiece) x).isIsolated()).count();
			eval += (isWhite ? 1 : -1) * 1 * game.getPieces(isWhite).stream().flatMap(x -> Arrays.stream(x.getAllowedMoves())).count();
		}
		return eval;
	}

	public static Score getBestMove(IChessGame board) {
		ArrayList<CompletableFuture<Score>> futures = new ArrayList<>();

		for (IPiece p : board.getPieces(board.isWhiteTurn()).stream().sorted(PIECE_COMPARATOR).toList()) {
			futures.add(CompletableFuture.supplyAsync(() -> {
				Vector bestMove = null;
				IChessGame copy = BoardUtils.copyBoard(board);
				IPiece copyPiece = copy.getPiece(p.getPosition());
				int bestScore = 0;
				for (Vector v : copyPiece.getAllowedMoves()) {
					copy.playPiece(copyPiece, v);

					int minimaxScore = minimax(copy, 1, Integer.MIN_VALUE, Integer.MAX_VALUE, true, copy.isWhiteTurn());

					copy.undoMove();
					if (bestMove == null
							|| (board.isWhiteTurn() ? bestScore < minimaxScore : bestScore > minimaxScore)) {
						bestMove = v;
						bestScore = minimaxScore;
					}
				}
				return new Score(p.getPosition(), bestMove, bestScore, 0);
			}));

		}
		try {
			List<Score> scoreList = new ArrayList<>(CompletableFuture.allOf(futures.toArray(new CompletableFuture[0])).thenApply(x -> futures.stream().map(CompletableFuture::join).filter(y -> y.to() != null).toList()).get());
			Collections.sort(scoreList, (s1, s2) -> (board.isWhiteTurn() ? 1 : -1) * Integer.compare(s2.score(), s1.score()));
			return scoreList.get(0);

		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
			return null;

		}

	}

	public static int minimax(IChessGame board, int depth, int alpha, int beta, boolean maximizingPlayer,
			boolean isWhite) {

		if (board.isCheckmate(true))
			return Integer.MAX_VALUE;

		if (board.isCheckmate(false))
			return Integer.MIN_VALUE;

		if (depth == 0) {
			return Heuristic.evaluateBoard(board);
		}

		if (maximizingPlayer == isWhite) {
			int maxEval = Integer.MIN_VALUE;
			for (IPiece p : board.getPieces(maximizingPlayer == isWhite)) {
				for (Vector v : p.getAllowedMoves()) {
					board.playPiece(p, v);
					Entry e = SQLiteHandler.getEntry(board, maximizingPlayer, isWhite, depth);
					int eval;
					if (e != null) {
						eval = e.score();
					} else {
						eval = minimax(board, depth - 1, alpha, beta, false, isWhite);
						SQLiteHandler.insertEntry(board, eval, depth, maximizingPlayer, isWhite);
					}
					board.undoMove();
					maxEval = Math.max(maxEval, eval);
					alpha = Math.max(alpha, eval);
					if (beta <= alpha)
						break;
				}
				if (beta <= alpha)
					break;
			}

			return maxEval;
		} else {
			int minEval = Integer.MAX_VALUE;
			for (IPiece p : board.getPieces(maximizingPlayer == isWhite)) {
				for (Vector v : p.getAllowedMoves()) {
					board.playPiece(p, v);
					Entry e = SQLiteHandler.getEntry(board, maximizingPlayer, isWhite, depth);
					int eval;
					if (e != null) {
						eval = e.score();
					} else {
						eval = minimax(board, depth - 1, alpha, beta, true, isWhite);
						SQLiteHandler.insertEntry(board, eval, depth, maximizingPlayer, isWhite);
					}
					board.undoMove();
					minEval = Math.min(minEval, eval);
					beta = Math.min(beta, eval);
					if (beta <= alpha)
						break;
				}
				if (beta <= alpha)
					break;
			}

			return minEval;
		}
	}

	public static record Score(Vector from, Vector to, int score, int depth, int row) implements Comparable<Score> {

		Score(Vector from, Vector to, int score, int depth) {
			this(from, to, score, depth, 0);
		}

		Score(Vector from, Vector to, int score) {
			this(from, to, score, 0);
		}

		@Override
		public int compareTo(Score o) {
			return Integer.compare(o.score, score);
		}
	}

	static class PieceComparator implements Comparator<IPiece> {
		List<Class<? extends Piece>> ORDER = List.of(QueenPiece.class, RookPiece.class, BishopPiece.class, KnightPiece.class, KingPiece.class, PawnPiece.class);

		@Override
		public int compare(IPiece o1, IPiece o2) {
			return Integer.compare(ORDER.indexOf(o2.getClass()), ORDER.indexOf(o1.getClass()));
		}

	}

}
