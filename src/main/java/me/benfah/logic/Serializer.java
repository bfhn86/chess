package me.benfah.logic;

import me.benfah.logic.piece.IPiece;
import me.benfah.logic.piece.PieceType;

public interface Serializer<T>
{
	Serializer<IChessGame> CHESS = new Serializer<>() {

		@Override
		public String serialize(IChessGame t) {
			String result = t.isWhiteTurn() ? "1 " : "0 ";
			for(int i = 0; i < t.getPieceArray().length; i++) {
				IPiece p = t.getPieceArray()[i];
				if(p == null) {
					result += "null ";
					continue;
				}
				result += "" + p.getSymbol() + (p.isWhite() ? "1" : "0") + p.getMoveCount() + " ";
			}
			return result;
		}

		@Override
		public ChessGame deserialize(String s) {
			String[] arr = s.split(" ");
			IPiece[] pieceArray = new IPiece[8 * 8];
			boolean whiteTurn = arr[0].equals("1");
			for(int i = 1; i < arr.length; i++) {
				String x = arr[i];
				if(x.equals("null")) {
					pieceArray[i] = null;
					continue;
				}
				PieceType<?> pt = PieceType.byNotation(x.charAt(0));
				IPiece p = pt.instance(x.charAt(1) == '1', null);
				p.setMoveCount(Integer.parseInt(x.substring(2)));
				pieceArray[i - 1] = p;
			}
			
			ChessGame board = new ChessGame(pieceArray);
			if(board.isWhiteTurn() != whiteTurn)
				board.toggleTurn();
			return board;
			
			
			
		}
	
	
	};
	
	public String serialize(T t);
	
	public T deserialize(String s);
	
}
