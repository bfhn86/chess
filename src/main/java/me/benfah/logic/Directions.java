package me.benfah.logic;

import java.util.Arrays;
import java.util.stream.Stream;

public class Directions {

	public static final Vector[] BISHOP_DIRECTIONS = new Vector[] { new Vector(1, 1), new Vector(1, -1), new Vector(-1, 1), new Vector(-1, -1) };

	public static final Vector[] ROOK_DIRECTIONS = new Vector[] { new Vector(1, 0), new Vector(-1, 0), new Vector(0, 1), new Vector(0, -1) };

	public static final Vector[] QUEEN_DIRECTIONS = Stream.concat(Arrays.stream(BISHOP_DIRECTIONS), Arrays.stream(ROOK_DIRECTIONS)).toArray(Vector[]::new);
}
