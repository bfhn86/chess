package me.benfah.logic;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;

import me.benfah.logic.piece.BishopPiece;
import me.benfah.logic.piece.IPiece;
import me.benfah.logic.piece.KingPiece;
import me.benfah.logic.piece.KnightPiece;
import me.benfah.logic.piece.PawnPiece;
import me.benfah.logic.piece.QueenPiece;
import me.benfah.logic.piece.RookPiece;

public class ChessGame implements IChessGame {

	public IPiece[] pieceArray = new IPiece[8 * 8];
	boolean whiteTurn = true;
	Deque<PieceMove> oldMoves = new ArrayDeque<>();

	Vector[] whiteCaptureMoves = new Vector[0];
	Vector[] blackCaptureMoves = new Vector[0];

	Vector[] whiteAllowedCaptureMoves = new Vector[0];
	Vector[] blackAllowedCaptureMoves = new Vector[0];

	int won = -1;

	public ChessGame() {
		setupGame();
	}

	public ChessGame(IPiece[] arr) {
		assert arr != null : "Array isn't allowed to be null";
		this.pieceArray = arr;
		Arrays.stream(pieceArray).filter(x -> x != null).forEach(x -> x.setBoard(this));
		calculateCheckMoves();
	}

	public void setupGame() {
		for (int i = 0; i < 8; i++) {
			setPiece(new Vector(i, 6), new PawnPiece(true, this));
		}

		for (int i = 0; i < 8; i++) {
			setPiece(new Vector(i, 1), new PawnPiece(false, this));
		}

		for (int i : new int[] { 7, 0 }) {
			boolean isWhite = i == 7;
			setPiece(new Vector(0, i), new RookPiece(isWhite, this));
			setPiece(new Vector(7, i), new RookPiece(isWhite, this));

			setPiece(new Vector(1, i), new KnightPiece(isWhite, this));
			setPiece(new Vector(6, i), new KnightPiece(isWhite, this));

			setPiece(new Vector(2, i), new BishopPiece(isWhite, this));
			setPiece(new Vector(5, i), new BishopPiece(isWhite, this));

			setPiece(new Vector(4, i), new QueenPiece(isWhite, this));
			setPiece(new Vector(3, i), new KingPiece(isWhite, this));
		}

		calculateCheckMoves();

	}

	public static int getBoardSize() {
		return 8;
	}

	public boolean undoMove() {
		if (!oldMoves.isEmpty()) {
			PieceMove pm = oldMoves.pop();
			IPiece movedPiece = getPiece(pm.to());
			setPiece(pm.from(), movedPiece);
			setPiece(pm.to(), pm.beatPiece());
			toggleTurn();
			movedPiece.setMoveCount(movedPiece.getMoveCount() - 1);
			onBoardUpdate();
			return true;
		}
		return false;
	}

	public Deque<PieceMove> getHistory() {
		return oldMoves;
	}

	public void setHistory(Deque<PieceMove> history) {
		assert history != null: "History can't be null";
		oldMoves = history;
	}

	public Vector getPiecePosition(IPiece p) {
		assert p != null : "Piece can't be null";
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				Vector res = new Vector(i, j);
				if (p.equals(getPiece(res)))
					return res;
			}
		}
		return null;
	}

	public void movePiece(IPiece p, Vector pos) {
		Vector currentPos = getPiecePosition(p);

		oldMoves.push(new PieceMove(currentPos, pos, getPiece(pos)));
		setPiece(currentPos, null);
		setPiece(pos, p);

	}

	public Vector[] getCaptureMoves(boolean isWhite) {
		return isWhite ? whiteCaptureMoves : blackCaptureMoves;
	}

	public Vector[] getAllowedCaptureMoves(boolean isWhite) {
		return isWhite ? whiteAllowedCaptureMoves : blackAllowedCaptureMoves;
	}

	public boolean isWhiteTurn() {
		return whiteTurn;
	}

	public void toggleTurn() {
		whiteTurn = !whiteTurn;
	}

	private void calculateCheckMoves() {
		whiteCaptureMoves = Arrays.stream(pieceArray).parallel().filter(x -> x != null && x.isWhite())
				.flatMap(x -> Arrays.stream(x.getCaptureMoves())).toArray(Vector[]::new);
		blackCaptureMoves = Arrays.stream(pieceArray).parallel().filter(x -> x != null && !x.isWhite())
				.flatMap(x -> Arrays.stream(x.getCaptureMoves())).toArray(Vector[]::new);

		whiteAllowedCaptureMoves = Arrays.stream(pieceArray).parallel().filter(x -> x != null && x.isWhite())
				.flatMap(x -> Arrays.stream(x.getAllowedCaptureMoves())).toArray(Vector[]::new);
		blackAllowedCaptureMoves = Arrays.stream(pieceArray).parallel().filter(x -> x != null && !x.isWhite())
				.flatMap(x -> Arrays.stream(x.getAllowedCaptureMoves())).toArray(Vector[]::new);

	}

	public void onBoardUpdate() {
		calculateCheckMoves();
		Arrays.stream(pieceArray).filter(x -> x != null).forEach(x -> x.onBoardPreUpdate());
		Arrays.stream(pieceArray).filter(x -> x != null).forEach(x -> x.onBoardUpdate());

		calculateCheckMoves();
	}

	public static int flatCoords(int x, int y) {
		int res = y * 8 + x;
		assert res >= 0 && res < 63 : "Coordinates must be valid";
		return res;
	}

	public static Vector toPosition(int boardPos) {
		return new Vector(boardPos % 8, boardPos / 8);
	}

	@Override
	public IPiece[] getPieceArray() {
		return pieceArray;
	}

	@Override
	public IPiece getPiece(Vector v) {
		return pieceArray[flatCoords(v.getX(), v.getY())];
	}

	@Override
	public void setPiece(Vector v, IPiece p) {
		pieceArray[flatCoords(v.getX(), v.getY())] = p;
	}

}
