package me.benfah.logic;

import java.util.Arrays;
import java.util.Deque;
import java.util.List;

import me.benfah.logic.Heuristic.Score;
import me.benfah.logic.piece.IPiece;
import me.benfah.logic.piece.PieceType;

public interface IChessGame {

	void setupGame();

	boolean undoMove();

	Vector getPiecePosition(IPiece p);

	default boolean isEmpty(Vector v) {
		return getPiece(v) == null;
	}

	IPiece getPiece(Vector v);

	void setPiece(Vector v, IPiece p);

	void movePiece(IPiece p, Vector v);

	IPiece[] getPieceArray();

	boolean isWhiteTurn();

	void toggleTurn();

	void onBoardUpdate();

	Deque<PieceMove> getHistory();

	void setHistory(Deque<PieceMove> history);

	Vector[] getCaptureMoves(boolean isWhite);

	Vector[] getAllowedCaptureMoves(boolean isWhite);

	@SuppressWarnings("unchecked")
	default <T extends IPiece> List<T> getPieces(PieceType<T> pt, boolean isWhite) {
		assert pt != null : "PieceType can't be null";
		return (List<T>) Arrays.stream(getPieceArray()).filter(x -> x != null && pt.is(x) && x.isWhite() == isWhite)
				.toList();
	}

	@SuppressWarnings("unchecked")
	default <T extends IPiece> List<T> getPieces(PieceType<T> pt) {
		assert pt != null : "PieceType can't be null";
		return (List<T>) Arrays.stream(getPieceArray()).filter(x -> x != null && pt.is(x)).toList();
	}

	default <T extends IPiece> T getFirstPiece(PieceType<T> pt, boolean isWhite) {
		return getPieces(pt, isWhite).get(0);
	}

	default List<IPiece> getPieces(boolean isWhite) {
		return Arrays.stream(getPieceArray()).parallel().filter(x -> x != null && x.isWhite() == isWhite).toList();
	}

	default boolean isCheckmate(boolean isWhite) {
		return getFirstPiece(PieceType.KING, isWhite).isInCheck() && getAllowedCaptureMoves(isWhite).length == 0;
	}

	default boolean isRemis(boolean isWhite) {
		return !getFirstPiece(PieceType.KING, isWhite).isInCheck() && getAllowedCaptureMoves(isWhite).length == 0;
	}

	default boolean isGameOver() {
		return isCheckmate(true) || isCheckmate(false) || isRemis(true) || isRemis(false);
	}

	default void playPiece(IPiece selectedPiece, Vector to) {
		assert selectedPiece != null && to != null && !isGameOver();
		assert Arrays.stream(selectedPiece.getAllowedMoves()).anyMatch(x -> x.equals(to));
		selectedPiece.onMove();
		movePiece(selectedPiece, to);
		toggleTurn();
		onBoardUpdate();
	}

	default Score getBestMove() {
		return Heuristic.getBestMove(this);
	}

}
