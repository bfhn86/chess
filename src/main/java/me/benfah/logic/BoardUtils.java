package me.benfah.logic;

import java.util.Arrays;

import me.benfah.logic.piece.Piece;

public class BoardUtils {
	public static IChessGame copyBoard(IChessGame board) {
		Piece[] copiedPieces = Arrays.stream(board.getPieceArray()).map(x -> {
			if (x == null)
				return null;
			try {
				return (Piece) x.getClass().getConstructor(boolean.class, IChessGame.class).newInstance(x.isWhite(),
						null);
			} catch (ReflectiveOperationException e) {
				e.printStackTrace();
			}
			return null;
		}).toArray(Piece[]::new);
		IChessGame copy = new ChessGame(copiedPieces);
		if (!board.isWhiteTurn())
			copy.toggleTurn();
		return copy;
	}
}
