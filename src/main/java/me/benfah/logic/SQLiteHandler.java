package me.benfah.logic;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class SQLiteHandler {
	static Connection c;

	public static void init() {
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:transposition_tables.db");
			String tableCreation = "CREATE TABLE IF NOT EXISTS transposition_table (id INTEGER PRIMARY KEY AUTOINCREMENT, board TEXT, score INTEGER, depth INTEGER, maximizingPlayer INTEGER, isWhite INTEGER)";
			Statement stmt = c.createStatement();

			stmt.executeUpdate(tableCreation);
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static int insertEntry(IChessGame board, int score, int depth, boolean maximizingPlayer, boolean isWhite) {
		String insert = "INSERT INTO transposition_table(board, score, depth, maximizingPlayer, isWhite) VALUES (?,?,?,?,?)";
		try {
			if (c == null || c.isClosed())
				return -1;

			PreparedStatement ps = c.prepareStatement(insert);
			ps.setString(1, Serializer.CHESS.serialize(board));
			ps.setInt(2, score);
			ps.setInt(3, depth);
			ps.setBoolean(4, maximizingPlayer);
			ps.setBoolean(5, isWhite);
			ps.executeUpdate();
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}

	}

	public static Entry getEntry(IChessGame board, boolean maximizingPlayer, boolean isWhite, int depth) {
		String select = "SELECT * FROM transposition_table WHERE board = ? AND maximizingPlayer = ? AND isWhite = ? AND depth >= ? ORDER BY depth DESC";
		try {
			if (c == null || c.isClosed())
				return null;

			PreparedStatement ps = c.prepareStatement(select);
			ps.setString(1, Serializer.CHESS.serialize(board));
			ps.setBoolean(2, maximizingPlayer);
			ps.setBoolean(3, isWhite);
			ps.setInt(4, depth);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				int id = rs.getRow();
				int score = rs.getInt(2);
				int retrievedDepth = rs.getInt(3);
				return new Entry(id, score, retrievedDepth, maximizingPlayer, isWhite);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static int updateEntry(IChessGame board, Entry s) {
		String insert = "UPDATE transposition_table SET score = ?, depth = ? WHERE id = ?";
		try {
			if (c == null || c.isClosed())
				return -1;

			PreparedStatement ps = c.prepareStatement(insert);
			ps.setInt(1, s.score());
			ps.setInt(2, s.depth());
			ps.setInt(3, s.id());
			ps.executeUpdate();
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}

	}

	public static record Entry(int id, int score, int depth, boolean maximizingPlayer, boolean isWhite) {}

}
