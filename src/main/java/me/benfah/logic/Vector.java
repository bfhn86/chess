package me.benfah.logic;

public class Vector {

	final int x, y;

	public Vector(int x, int y) {

		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public Vector add(int x, int y) {
		return new Vector(this.x + x, this.y + y);
	}

	public Vector add(Vector v) {
		return new Vector(this.x + v.getX(), this.y + v.getY());
	}

	public Vector sub(Vector v) {
		return new Vector(this.x - v.getX(), this.y - v.getY());
	}

	public Vector multiply(int factor) {
		return new Vector(this.x * factor, this.y * factor);
	}

	public boolean isValid() {
		return x >= 0 && x < 8 && y >= 0 && y < 8;
	}

	public boolean isPositivelyCollinear(Vector v) {
		if (x * v.y - y * v.x != 0) {
			return false;
		}
		return (x * v.x >= 0 && y * v.y >= 0);
	}

	public boolean isCollinear(Vector v) {
		return (x * v.y - y * v.x) == 0;
	}

	public double getFactorOf(Vector v) {
		if (v.x == 0 && v.y == 0)
			throw new IllegalArgumentException("Null vector not allowed");

		try {
			return v.x != 0 ? this.x / v.x : this.y / v.y;

		} catch (ArithmeticException e) {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj.getClass() != getClass())
			return false;
		Vector pos = (Vector) obj;
		return pos.getX() == getX() && pos.getY() == getY();
	}

}
