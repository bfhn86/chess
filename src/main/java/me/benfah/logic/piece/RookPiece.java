package me.benfah.logic.piece;

import me.benfah.logic.Directions;
import me.benfah.logic.IChessGame;
import me.benfah.logic.Vector;

public class RookPiece extends VectorPiece {

	public RookPiece(boolean isWhite, IChessGame board) {
		super(isWhite, board, 'r');
	}

	@Override
	public Vector[] getDirections() {
		return Directions.ROOK_DIRECTIONS;
	}

}
