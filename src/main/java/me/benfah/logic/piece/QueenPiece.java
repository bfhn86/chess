package me.benfah.logic.piece;

import me.benfah.logic.Directions;
import me.benfah.logic.IChessGame;
import me.benfah.logic.Vector;

public class QueenPiece extends VectorPiece {

	public QueenPiece(boolean isWhite, IChessGame board) {
		super(isWhite, board, 'q');
	}

	@Override
	public Vector[] getDirections() {
		return Directions.QUEEN_DIRECTIONS;
	}

}
