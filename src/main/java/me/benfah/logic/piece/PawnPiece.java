package me.benfah.logic.piece;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import me.benfah.logic.IChessGame;
import me.benfah.logic.Vector;

public class PawnPiece extends Piece {

	Vector[] captureMoves = new Vector[0];

	public PawnPiece(boolean isWhite, IChessGame board) {
		super(isWhite, board, 'p');
	}

	@Override
	public Vector[] calculateMoves() {

		List<Vector> result = new ArrayList<>();

		Vector currentPos = getPosition();
		int prefactor = isWhite ? -1 : 1;

		for (int i : new int[] { 1, -1 }) {
			Vector side = currentPos.add(i, prefactor);
			if (side.isValid())
				result.add(side);
		}

		return result.toArray(new Vector[0]);
	}

	@Override
	public Vector[] calculateNonCaptureMoves() {
		List<Vector> result = new ArrayList<>();

		int prefactor = isWhite ? -1 : 1;

		Vector forward = getPosition().add(0, prefactor);
		if (forward.isValid() && board.isEmpty(forward)) {
			result.add(forward);

			Vector forwardSecond = forward.add(0, prefactor);

			if (moveCount == 0 && forwardSecond.isValid() && board.isEmpty(forwardSecond)) {
				result.add(forwardSecond);
			}
		}

		return result.toArray(new Vector[0]);
	}

	public Vector[] calculateCaptureMoves() {

		List<Vector> result = new ArrayList<>();

		Vector currentPos = getPosition();
		int prefactor = isWhite ? -1 : 1;

		for (int i : new int[] { 1, -1 }) {
			Vector side = currentPos.add(i, prefactor);
			if (side.isValid())
				result.add(side);
		}

		return result.toArray(new Vector[0]);
	}

	public boolean isIsolated() {
		boolean result = true;
		Vector pos = getPosition();
		for (int i = 0; i < 8; i++)
			for (int j : new int[] { 1, -1 }) {
				Vector v = new Vector(pos.getX() + j, i);
				if (v.isValid())
					result &= !(board.getPiece(v) instanceof PawnPiece);
			}
		return result;
	}

	public boolean isDoubled() {
		Vector[] positions = new Vector[] { getPosition().add(0, 1), getPosition().add(0, -1) };
		return Arrays.stream(positions).anyMatch(x -> x.isValid() && board.getPiece(x) instanceof PawnPiece);
	}

	public boolean isBlocked() {
		Vector pos = getPosition().add(0, isWhite ? -1 : 1);
		return pos.isValid() && !board.isEmpty(pos);
	}

	@Override
	public boolean isMoveAllowed(Vector v) {
		return !board.isEmpty(v) && board.getPiece(v).isWhite() != isWhite();
	}

}