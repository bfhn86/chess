package me.benfah.logic.piece;

import java.util.Arrays;

import me.benfah.logic.IChessGame;
import me.benfah.logic.Vector;

public interface IPiece {

	IChessGame getBoard();

	void setBoard(IChessGame board);

	Vector[] getAllowedCaptureMoves();

	Vector[] getAllowedMoves();

	Vector[] getCaptureMoves();

	Vector[] getAllowedNonCaptureMoves();

	String getGraphic();

	char getSymbol();

	void onMove();

	boolean isMoveAllowed(Vector v);

	boolean isWhite();

	void setMoveCount(int i);

	int getMoveCount();

	Vector getProtectorDirection();

	void setProtectorDirection(Vector v);

	void onBoardPreUpdate();

	void onBoardUpdate();

	default Vector getPosition() {
		return getBoard().getPiecePosition(this);
	}

	default IPiece getCheckPiece() {
		Vector currentPos = getPosition();
		return getBoard().getPieces(!isWhite()).stream().parallel()
				.filter(x -> Arrays.stream(x.getCaptureMoves()).anyMatch(y -> y.equals(currentPos))).findAny()
				.orElse(null);
	}

	default Vector[] checkFilter(Vector[] v) {
		KingPiece king = getBoard().getFirstPiece(PieceType.KING, isWhite());

		if (king.getCheckPiece() instanceof VectorPiece) {
			return ((VectorPiece) king.getCheckPiece()).filterCheckTrajectory(v);
		} else {
			return Arrays.stream(v).filter(x -> x.equals(king.getCheckPiece().getPosition())).toArray(Vector[]::new);
		}
	}

	default Vector[] protectorFilter(Vector[] v) {
		Vector pos = getPosition();
		return Arrays.stream(v).filter(x -> x.sub(pos).isCollinear(getProtectorDirection())).toArray(Vector[]::new);
	}

	default Vector[] filter(Vector[] v) {
		if (getBoard().getFirstPiece(PieceType.KING, isWhite()).isInCheck() && getProtectorDirection() != null)
			return protectorFilter(checkFilter(v));
		else if (getProtectorDirection() != null)
			return protectorFilter(v);
		else if (getBoard().getFirstPiece(PieceType.KING, isWhite()).isInCheck())
			return checkFilter(v);
		return v;
	}

}
